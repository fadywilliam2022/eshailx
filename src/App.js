// import logo from './logo.svg';
import { createBrowserHistory } from 'history'

import './App.css';
import Header from './components/common/Header'
import Navbar from './components/common/Navbar'
import Footer from  './components/common/Footer'
import Sidebar from './components/common/Sidebar';
import Statistics from './components/dashboard/Statistics';
// import { getInfo } from '../src/store/actions'
import React, { useEffect } from 'react';

import { useSelector, useDispatch } from 'react-redux'
// import AuthService from "./services/auth.service";

function Dashboard() {
     const history = createBrowserHistory()
    const store = useSelector(state => state)    
    // const currentUser = AuthService.getCurrentUser();
    console.log('ddd');
    console.log(store);
// console.log(currentUser);
    
    const dispatch = useDispatch()
    useEffect(() => {    
     // dispatch(getInfo())
    }, [dispatch])
    
    if(store !== undefined){
        if(!store.auth.isLoggedIn){
            history.push("/login");
            window.location.reload();
        }
    }
  return (
    <div>
                    {/* <div id="load_screen"> 
                            <div class="loader"> 
                                <div class="loader-content">
                                    <div class="spinner-grow align-self-center"></div>
                                </div>
                            </div>
                    </div>
                     */}
                   
        <Header />
        <Navbar />    
    <div class="main-container" id="container">
        <div class="overlay"></div>
        <div class="search-overlay"></div>
            <Sidebar />       
            <div id="content" class="main-content">
                <div class="layout-px-spacing">      
                        <div class="row layout-top-spacing">                                                 
                        {/* <Statistics />                                                                        */}
                        </div>            
                </div>            
            <Footer />
        </div>
    </div>
    </div>
  );
}

export default Dashboard;
