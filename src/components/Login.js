import React, { useState} from "react";
import { Navigate } from 'react-router-dom';

import Form from "react-validation/build/form";
import Input from "react-validation/build/input";
// import CheckButton from "react-validation/build/button";

// import { connect } from "react-redux";
import { login } from "../store/actions/auth";
import { useSelector, useDispatch } from 'react-redux'



const Login = () => {
const [username,setUsername] = useState("")
const [password,setPassword] = useState("")
const dispatch = useDispatch()
const store = useSelector(state => state)    

const handleSubmit = (e) => {
  console.log('submit button');
  console.log(store)
  e.preventDefault();
  // this.setState({
  //   loading: true,
  // });
    dispatch(
      login(username,password,        
    )
    );

};
let message = '';
if (store!= undefined) {
  message = store.message.message
  console.log(store)
  if(store.auth.isLoggedIn){
    return <Navigate to="/" />;
  }
}
    return (
        <div class="form-container outer">
        <div class="form-form">
            <div class="form-form-wrap">
                <div class="form-container">
                    <div class="form-content">

                        <h1 class="">Sign in</h1>                        
                        <Form onSubmit={(e)=>handleSubmit(e)}>
                        {/* <form class="text-left"> */}
                            <div class="form">

                                <div id="username-field" class="field-wrapper input">
                                <div class="d-flex justify-content-between">
                                    <label for="username">Mobile</label>
                                    </div>
                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-user"><path d="M20 21v-2a4 4 0 0 0-4-4H8a4 4 0 0 0-4 4v2"></path><circle cx="12" cy="7" r="4"></circle></svg>
                                    <Input type="text" class="form-control" placeholder="e.g 1140586879"                                         
                                         name="username"
                                         id = "username"
                                         value={username}
                                         
                                         onChange={(e) => setUsername(e.target.value)}                                       
                                    />
                                </div>

                                <div id="password-field" class="field-wrapper input mb-2">
                                    <div class="d-flex justify-content-between">
                                        <label for="password">PASSWORD</label>
                                        
                                    </div>
                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-lock"><rect x="3" y="11" width="18" height="11" rx="2" ry="2"></rect><path d="M7 11V7a5 5 0 0 1 10 0v4"></path></svg>
                                    <Input 
                                    type="password"
                                    className="form-control"
                                    name="password"
                                    id = "password"  
                                                                  
                                    onChange={(e) => setPassword(e.target.value)}
                                    value={password}
                                     />
                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" id="toggle-password" class="feather feather-eye"><path d="M1 12s4-8 11-8 11 8 11 8-4 8-11 8-11-8-11-8z"></path><circle cx="12" cy="12" r="3"></circle></svg>
                                </div>
                                {/* <div class="d-sm-flex justify-content-between">
                                    <div class="field-wrapper">
                                        <button type="submit" class="btn btn-primary" value="">Log In</button>
                                    </div>
                                </div> */}

    

                                                      
                            </div>
                        {/* </form> */}
                        <div className="form-group">
              <button
                className="btn btn-primary btn-block"
                // disabled={this.state.loading}
              >
                {/* {this.state.loading && (
                  <span className="spinner-border spinner-border-sm"></span>
                )} */}
                <span>Login</span>
              </button>
            </div>

            {message && (
              <div className="form-group">
                <div className="alert alert-danger" role="alert">
                  {message}
                </div>
              </div>
            )}
            
                        </Form>

                    </div>                    
                </div>
            </div>
        </div>
    </div>
    );
  }


export default Login