import React from "react";
import { Card } from "react-bootstrap";

const StepFinal = ({ values }) => {

    //destructuring the object from values
  const { mobile, password, name, userCurrncy,hotLine,pointPrice,currency,commissionPercentage,taxRegistrationNumber,categoryId } = values;
  return (
    <>
    <div class="alert alert-success" role="alert">
        Save merchant details
</div>
      <Card style={{ marginTop: 100, textAlign: "left" }}>
        <Card.Body>
          <p>
            <strong>Mobile :</strong> {mobile}{" "}
          </p>
          <p>
            <strong>Password :</strong> {password}{" "}
          </p>
          <p>
            <strong>Name :</strong> {name}{" "}
          </p>
          <p>
            <strong>userCurrncy :</strong> {userCurrncy}{" "}
          </p>
          <p>
            <strong>Hotline :</strong> {hotLine}{" "}
          </p>
          <p>
            <strong>PointPrice :</strong> {pointPrice}{" "}
          </p>
          <p>
            <strong>Currency :</strong> {currency}{" "}
          </p>
          <p>
            <strong>CommissionPercentage :</strong> {commissionPercentage}{" "}
          </p>
          <p>
            <strong>TaxRegistrationNumber :</strong> {taxRegistrationNumber}{" "}
          </p>
          <p>
            <strong>Category :</strong> {categoryId}{" "}
          </p>
        </Card.Body>
      </Card>
    </>
  );
};

export default StepFinal;
