import React, { useState } from "react";
import { Form, Card, Button } from "react-bootstrap";
import validator from "validator";
import { signupMerchant } from '../store/actions'
import { useSelector, useDispatch } from 'react-redux'

// creating functional component ans getting props from app.js and destucturing them
const StepOne = ({ nextStep, handleFormData, values }) => {
  //creating error state for validation
  const [error, setError] = useState(false);
  
  const dispatch = useDispatch()
  const store = useSelector(state => state) 
  let message ='';
  let flag = false
  const next = () => {
    nextStep();
  };
  // after form submit validating the form data using validator
  const submitFormData = (e) => {
    e.preventDefault();

    // checking if value of first name and last name is empty show error else take to step 2
    if (
      validator.isEmpty(values.mobile) ||
      validator.isEmpty(values.password) || 
      validator.isEmpty(values.name) || validator.isEmpty(values.userCurrncy)
    ) {
      setError(true);
    } else {
      dispatch(
        signupMerchant(values.name,values.mobile,values.password,values.userCurrncy)
      );
      nextStep();

    // if(store !=undefined){
    //   if(store.message.message !=="Success signup"){   
    //     message = store.message.message
    //     console.log(store.message.message)
    //   }else{
    //     flag = true;
    //    // nextStep();
    //   }
    // }
     
    }
  };
//   console.log(flag)
// let elem ='';
//   if (store.message.message ==="Success signup") {
//     elem= <Button variant="primary" type="button" onClick={next()}>
//    next
//   </Button>
//   }else{
//    elem =<Button variant="primary" type="submit" >
//   Continue
// </Button>
// }

  return (
    <div>
 {/* {store.message.message && (
              <div className="form-group">
                <div className="alert alert-danger" role="alert">
                  {store.message.message}
                </div>
              </div>
            )} */}
      <Card style={{ marginTop: 100 }}>
        <Card.Body>
          <Form onSubmit={submitFormData}>
          <Form.Group className="mb-3">
              <Form.Label>Name</Form.Label>
              <Form.Control
                style={{ border: error ? "2px solid red" : "" }}
                type="text"
                name="name"
                placeholder="Name"
                onChange={handleFormData("name")}
              />
              {error ? (
                <Form.Text style={{ color: "red" }}>
                  This is a required field
                </Form.Text>
              ) : (
                ""
              )}
            </Form.Group>

            <Form.Group className="mb-3">
              <Form.Label>Mobile</Form.Label>
              <Form.Control
                style={{ border: error ? "2px solid red" : "" }}
                name="mobile"
                defaultValue={values.mobile}
                type="text"
                placeholder="Mobile"
                onChange={handleFormData("mobile")}
              />
              {error ? (
                <Form.Text style={{ color: "red" }}>
                  This is a required field
                </Form.Text>
              ) : (
                ""
              )}
            </Form.Group>
            <Form.Group className="mb-3">
              <Form.Label>Password</Form.Label>
              <Form.Control
                style={{ border: error ? "2px solid red" : "" }}
                name="password"
                defaultValue={values.password}
                type="text"
                placeholder="Password"
                onChange={handleFormData("password")}
              />
              {error ? (
                <Form.Text style={{ color: "red" }}>
                  This is a required field
                </Form.Text>
              ) : (
                ""
              )}
            </Form.Group>
            <Form.Group className="mb-3">
              <Form.Label>User Currency</Form.Label>
              <Form.Control
                style={{ border: error ? "2px solid red" : "" }}
                type="text"
                name="userCurrncy"
                placeholder="User Currency"
                onChange={handleFormData("userCurrncy")}
              />
              {error ? (
                <Form.Text style={{ color: "red" }}>
                  This is a required field
                </Form.Text>
              ) : (
                ""
              )}
            </Form.Group>
            
           
            <Button variant="primary" type="submit" >
   Continue
 </Button>

          </Form>
        </Card.Body>
      </Card>
    </div>
  );
};

export default StepOne;
