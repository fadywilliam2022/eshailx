import React, {useEffect, useState } from "react";

import { Form, Card, Button } from "react-bootstrap";
import validator from "validator";
import { useSelector, useDispatch } from 'react-redux'

import { CreateMerchantProfile,listCategory } from '../store/actions'

// creating functional component ans getting props from app.js and destucturing them
const StepTwo = ({ nextStep, handleFormData, prevStep, values }) => {
   //creating error state for validation
  const [error, setError] = useState(false);
  const dispatch = useDispatch()
  const store = useSelector(state => state)    
  useEffect(() => {   
    // console.log(getInfo()); 
  dispatch(listCategory(localStorage.getItem('token')))
}, [dispatch])


  let merchantToken = '';
  let merchantId = "";
  if(store.eshailx.sign_up_merchant.data !== undefined){
    merchantToken =localStorage.getItem('token')
    merchantId = store.eshailx.sign_up_merchant.data.id;
  }

    // after form submit validating the form data using validator
  const submitFormData = (e) => {
    e.preventDefault();

     // checking if value of first name and last name is empty show error else take to next step
    if (validator.isEmpty(values.pointPrice) || validator.isEmpty(values.currency)
      || validator.isEmpty(values.commissionPercentage )
      || validator.isEmpty(values.taxRegistrationNumber  )
      || validator.isEmpty(values.categoryId)


    ) {
      setError(true);
    } else {
      dispatch(
        //token,merchantId ,hotLine,pointPrice,currency,commissionPercentage,taxRegistrationNumber,categoryId
        CreateMerchantProfile(merchantToken,merchantId,values.hotLine,values.pointPrice,values.currency,values.commissionPercentage,values.taxRegistrationNumber,values.categoryId)
      );
      nextStep();
    }
  };
let categoryList= [];
categoryList.push( <option selected>
  Select Category
</option>)
  if(store.eshailx.categories.data !== undefined){
      store.eshailx.categories.data.rows.map(function(item, i){
        categoryList.push(<option key={i} value={item.id}>
             {item.name}
          </option>)
      })
  }
  return (
    <>
      <Card style={{ marginTop: 100 }}>
        <Card.Body>
          <Form onSubmit={submitFormData}>
          <Form.Group className="mb-3">
              <Form.Label>Category</Form.Label>
              <Form.Select aria-label="Default select example" 
              style={{ border: error ? "2px solid red" : "" }}
              className="form-control" name="categoryId"  onChange={handleFormData("categoryId")}>
              { categoryList }
            </Form.Select>
            {error ? (
                <Form.Text style={{ color: "red" }}>
                  This is a required field
                </Form.Text>
              ) : (
                ""
              )}
            
            </Form.Group>
            <Form.Group className="mb-3">
              <Form.Label>HotLine</Form.Label>
              <Form.Control
                style={{ border: error ? "2px solid red" : "" }}
                type="text"
                name="hotLine"
                placeholder="HotLine"
                onChange={handleFormData("hotLine")}
              />
              {error ? (
                <Form.Text style={{ color: "red" }}>
                  This is a required field
                </Form.Text>
              ) : (
                ""
              )}
            </Form.Group>
            <Form.Group className="mb-3">
              <Form.Label>Point Price</Form.Label>
              <Form.Control
                style={{ border: error ? "2px solid red" : "" }}
                type="text"
                name="pointPrice"
                placeholder="Point Price"
                onChange={handleFormData("pointPrice")}
              />
              {error ? (
                <Form.Text style={{ color: "red" }}>
                  This is a required field
                </Form.Text>
              ) : (
                ""
              )}
            </Form.Group>
            <Form.Group className="mb-3">
              <Form.Label>Currency</Form.Label>
              <Form.Control
                style={{ border: error ? "2px solid red" : "" }}
                type="text"
                name="currency"
                placeholder="Currency"
                onChange={handleFormData("currency")}
              />
              {error ? (
                <Form.Text style={{ color: "red" }}>
                  This is a required field
                </Form.Text>
              ) : (
                ""
              )}
            </Form.Group>
            <Form.Group className="mb-3">
              <Form.Label>Commission %</Form.Label>
              <Form.Control
                style={{ border: error ? "2px solid red" : "" }}
                type="text"
                name="commissionPercentage"
                placeholder="ex 1.5%"
                onChange={handleFormData("commissionPercentage")}
              />
              {error ? (
                <Form.Text style={{ color: "red" }}>
                  This is a required field
                </Form.Text>
              ) : (
                ""
              )}
            </Form.Group>
            <Form.Group className="mb-3">
              <Form.Label>Tax Registration Numer</Form.Label>
              <Form.Control
                style={{ border: error ? "2px solid red" : "" }}
                type="text"
                name="taxRegistrationNumber"
                placeholder="Tax Registration Number"
                onChange={handleFormData("taxRegistrationNumber")}
              />
              {error ? (
                <Form.Text style={{ color: "red" }}>
                  This is a required field
                </Form.Text>
              ) : (
                ""
              )}
            </Form.Group>

            {/* <Form.Group className="mb-3">
              <Form.Label>Category Id</Form.Label>
              <Form.Control
                style={{ border: error ? "2px solid red" : "" }}
                type="text"
                name="categoryId"
                placeholder="Category Id"
                onChange={handleFormData("categoryId")}
              />
              {error ? (
                <Form.Text style={{ color: "red" }}>
                  This is a required field
                </Form.Text>
              ) : (
                ""
              )}
            </Form.Group> */}

            <div style={{ display: "flex", justifyContent: "space-around" }}>
              <Button variant="primary" onClick={prevStep}>
                Previous
              </Button>

              <Button variant="primary" type="submit">
                Submit
              </Button>
            </div>
          </Form>
        </Card.Body>
      </Card>
    </>
  );
};

export default StepTwo;
