import React,{useEffect,Fragment,useState} from "react";
import Header from '../common/Header'
import Navbar from '../common/Navbar'
import Footer from  '../common/Footer'
import Sidebar from '../common/Sidebar';
import { useSelector, useDispatch } from 'react-redux'
// import DataTable from 'react-data-table-component';
import { Container, Row, Col } from "react-bootstrap";
import { useParams } from "react-router-dom";
import { Table, Card } from "react-bootstrap";
import ReactDatatable from '@mkikets/react-datatable';
import { createBrowserHistory } from 'history'

import "./style.css";
// import { ArrowRight } from 'react-bootstrap-icons';

// import SuccessPopup from 'react-success-popup'

import { addEmployee,getBranchDetails,listEmoloyees } from '../../store/actions'
import Form from "react-validation/build/form";
import Input from "react-validation/build/input";


// import data from '../data.json';
import { Button, Modal } from "react-bootstrap";
// import Multistep from 'react-multistep'

function BranchDetails (){
  const dispatch = useDispatch()
  const store = useSelector(state => state)    
  const [show, setShow] = useState(false);    
    const [message,setMessage] = useState("");
    const [title,setTitle] = useState("");
    const history = createBrowserHistory()



    // - Employee data
    const [fName,setFName] = useState("");
    const [lName,setLName] = useState("");
    const [mobile,setMobile] = useState("");
    const [password,setPassword] = useState("");
    const [rule,setRule] = useState("");

// branch details
    let {branchName,address,phone,lat,long} = '';
    
    const [pageNum,setPageNum]=useState(0);
    const [pageSize,setPageSize]=useState(10);
    const [loading,setLoading]=useState(true)
    let totalEmployee = 0
    const config = {
      page_size: pageSize,
      length_menu: [10, 20, 50],
      show_filter: true,
      show_pagination: true,
      pagination: 'advance',
      language: {
        loading_text: "loading..."
    },
      button: {
          excel: false,
          csv: false,
          print:false
      }
  }
  const tableChangeHandler= data => {
    console.log(data)
    setPageNum(data.page_number-1)
    setPageSize(data.page_size)


  
}
const handleDeleteKey = (category)=>{ 
    // alert(category.id)
    // dispatch(
    //     deleteCategory(localStorage.getItem('token'),category.id)
    //   );
}
    const handleHide = () => setShow(false);
    // const [name,SetName] = useState("")

    const { id,merchantId } =  useParams();
    const handleSubmit = (e) => {
        console.log('dsdasd')
        console.log(id)
        console.log('submit button');
     
        console.log(store)
        // setShow(false);
        e.preventDefault();
        
        dispatch(
            addEmployee(localStorage.getItem('token'),id,mobile,password,fName,lName,rule)
        );
        dispatch(listEmoloyees(localStorage.getItem('token'),id,merchantId))


          setMessage('Success save Branch');
      
      };
    useEffect(() => {   
        console.log(id)
        console.log(merchantId)

        // console.log(getInfo()); 
      dispatch(getBranchDetails(localStorage.getItem('token'),id))
      dispatch(listEmoloyees(localStorage.getItem('token'),id,merchantId))

      

      setTimeout(() => {
        setLoading(false)
    }, 1000);

    }, [pageNum,pageSize,loading,id,merchantId])
    let element_employee = []; 
    console.log('step')      
    console.log(store)
   
    const showBranchDetails = ()=>{
        history.push("/login");
        window.location.reload();
    }
    const handleShow = ()=>{ 
        setShow(true);
        setTitle(`Add New Employee`);
        setFName('')
        setMessage('');
        }
  
        const columns = [
            {
              text: 'First name',
              key: 'fName',
              sortable: true,
              cell: record => {
              return  record.Action.fName
              }
            },
           
            {
              text: 'Last name',
              key: 'lName',
              sortable: true,
              cell: record => {
                return record.Action.fName
      
                }
            },
            // {
            //   text: 'Mobile',
            //   key: 'mobile',
            //   sortable: true,
            // },
            // {
            //     text: 'Password',
            //     key: 'password',
            //     sortable: true,
            //   },
              {
                text: 'Rule',
                key: 'rule',
                sortable: true,
                cell: record => {
                    return record.Action.rule
          
                    }
              },
           
            // {
            //     text: "Actions",         
            //     key: "action",
            //     className: "action",
            //     width: 100,
            //     align: "left",
            //     sortable: false,
            //     cell: record => {
            //         if (record.Action) {
                     
            //             return (
            //                 <Fragment>   
                                                  
                               
                               
            //                         { <button
            //                             onClick={() => {
            //                                 // editCategory(record.Action);
            //                             }}
            //                             type="submit"
            //                             style={{padding:"3px",margin:"3px"}}
            //                         >
            //                           <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-pencil-fill" viewBox="0 0 16 16">
            //                           <path d="M12.854.146a.5.5 0 0 0-.707 0L10.5 1.793 14.207 5.5l1.647-1.646a.5.5 0 0 0 0-.708l-3-3zm.646 6.061L9.793 2.5 3.293 9H3.5a.5.5 0 0 1 .5.5v.5h.5a.5.5 0 0 1 .5.5v.5h.5a.5.5 0 0 1 .5.5v.5h.5a.5.5 0 0 1 .5.5v.207l6.5-6.5zm-7.468 7.468A.5.5 0 0 1 6 13.5V13h-.5a.5.5 0 0 1-.5-.5V12h-.5a.5.5 0 0 1-.5-.5V11h-.5a.5.5 0 0 1-.5-.5V10h-.5a.499.499 0 0 1-.175-.032l-.179.178a.5.5 0 0 0-.11.168l-2 5a.5.5 0 0 0 .65.65l5-2a.5.5 0 0 0 .168-.11l.178-.178z"/>
            //                           </svg>
    
    
            //                         </button>
            //                      }
            //                     </Fragment>
            //             );
            //         }
            //     }
            // }
          ];
         
          if(store.eshailx.branch_details.data !==undefined){
            branchName = store.eshailx.branch_details.data.branchName
            phone = store.eshailx.branch_details.data.phone
            lat = store.eshailx.branch_details.data.lat
            long = store.eshailx.branch_details.data.long
            address = store.eshailx.branch_details.data.address
            }
    // console.log(store.eshailx.merchants.data);
      
        if(store.eshailx.employee_list.data !== undefined){
            totalEmployee = store.eshailx.employee_list.data.count

            store.eshailx.employee_list.data.rows.map(function(item, i){
                element_employee.push({'Firstname':item.fName,'Lastname':item.lName,'Rule':item.rule,'Action':item})
            })
        }

        

return (
    <div>
                    {/* <div id="load_screen"> 
                            <div class="loader"> 
                                <div class="loader-content">
                                    <div class="spinner-grow align-self-center"></div>
                                </div>
                            </div>
                    </div>
                     */}
                   
        <Header />
        <Navbar />    
    
    <div class="main-container" id="container">
       
        <div class="overlay"></div>
        <div class="search-overlay"></div>
            <Sidebar />       
            
            <div id="content" class="main-content">
                <div class="layout-px-spacing">      
                        <div class="row layout-top-spacing">      

<>

<div class="col-xl-12 col-lg-12 col-sm-12  layout-spacing">
                    <Row>
                        <Col sm={12}>
                        <h4>Branch Info:</h4>
                        <Table striped>
                            <thead>
                                <tr>
                                    <th>Branch Name</th>
                                    <td>{branchName}</td>
                                </tr>
                               

                                <tr>
                                    <th>Phone</th>
                                    <td>{phone}</td>
                                </tr><tr>
                                    <th>Latitude</th>
                                    <td>{lat}</td>
                                </tr>
                                <tr>
                                    <th>Longitude</th>
                                    <td>{long}</td>
                                </tr>
                                <tr>
                                    <th>Address</th>
                                    <td>{address}</td>
                                </tr>
                             
                               
                            </thead>

                        </Table>
                    </Col>         
        </Row>
        <Button variant="success" onClick={handleShow}> New Employee<div /></Button>

        <div class="widget-content widget-content-area br-6">   
        <Modal show={show}>
        <Modal.Header>
          <Modal.Title>{title}</Modal.Title>
        </Modal.Header>
        <Modal.Body>
        {message && (
              <div className="form-group">
                <div className={`alert alert-${alert}`} role="alert">
                  {message}
                </div>
              </div>
            )}

            <Form onSubmit={(e)=>handleSubmit(e)}>   
                       <div class="form">
                       <div id="name-field" class="field-wrapper input">
                               <div class="d-flex justify-content-between">
                                   <label for="name">First name</label>
                               </div>                                    
                                   <Input type="text" class="form-control" placeholder=""                                         
                                       name="fName"
                                       id = "fName"
                                       value={fName}  
                                       required="required"                                                                               
                                       onChange={(e) => setFName(e.target.value)}                                       
                                   />
                           </div> 
                          
                           
                           <div id="name-field" class="field-wrapper input">
                               <div class="d-flex justify-content-between">
                                   <label for="name">Last name</label>
                               </div>                                    
                                   <Input type="text" class="form-control" placeholder=""                                         
                                       name="lName"
                                       id = "lName"
                                       value={lName}  
                                       required="required"                                                                               
                                       onChange={(e) => setLName(e.target.value)}                                       
                                   />
                           </div> 
                           <div id="name-field" class="field-wrapper input">
                               <div class="d-flex justify-content-between">
                                   <label for="name">Mobile</label>
                               </div>                                    
                                   <Input type="text" class="form-control" placeholder=""                                         
                                       name="mobile"
                                       id = "mobile"
                                       value={mobile}  
                                       required="required"                                                                               
                                       onChange={(e) => setMobile(e.target.value)}                                       
                                   />
                           </div> 
                           <div id="name-field" class="field-wrapper input">
                               <div class="d-flex justify-content-between">
                                   <label for="name">Password</label>
                               </div>                                    
                                   <Input type="text" class="form-control" placeholder=""                                         
                                       name="password"
                                       id = "password"
                                       value={password}  
                                       required="required"                                                                               
                                       onChange={(e) => setPassword(e.target.value)}                                       
                                   />
                           </div> 
                           
                         
                           <div id="name-field" class="field-wrapper input">
                               <div class="d-flex justify-content-between">
                                   <label for="name">Rule</label>
                               </div>                                    
                                   <Input type="text" class="form-control" placeholder=""                                         
                                       name="rule"
                                       id = "rule"
                                       value={rule}  
                                       required="required"                                                                               
                                       onChange={(e) => setRule(e.target.value)}                                       
                                   />
                           </div> 
                       </div>   
                       <button class="btn btn-primary submit-fn mt-2" type="submit">Save</button>

   </Form>    
                           
                     
        </Modal.Body>
        <Modal.Footer>
          <Button variant="btn btn submit-fn mt-2" onClick={handleHide}>Close</Button>
        </Modal.Footer>
      </Modal>
                <ReactDatatable
                    config={config}
                    records={element_employee}
                    columns={columns}
                    dynamic={false}
                    total_record={totalEmployee}
                    loading={loading}
                    onChange={tableChangeHandler} />

    </div>
</div>

</>
  
</div>            
                </div>            
            <Footer />
        </div>
    </div>
    </div>
);
                    }

 export default BranchDetails;
