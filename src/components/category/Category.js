import React,{useEffect,Fragment,useState} from "react";
import Header from '../common/Header'
import Navbar from '../common/Navbar'
import Footer from  '../common/Footer'
import Sidebar from '../common/Sidebar';
import { useSelector, useDispatch } from 'react-redux'
// import DataTable from 'react-data-table-component';
import ReactDatatable from '@mkikets/react-datatable';

import Form from "react-validation/build/form";
import Input from "react-validation/build/input";

// import { ArrowRight } from 'react-bootstrap-icons';

// import SuccessPopup from 'react-success-popup'

import { addCategory,listCategory,deleteCategory,updateCategory } from '../../store/actions'




// import data from '../data.json';
import { Button, Modal } from "react-bootstrap";


function Category (){
    const dispatch = useDispatch()
    const store = useSelector(state => state) 
    
    const [show, setShow] = useState(false);    
    const [message,setMessage] = useState("");
    const [title,setTitle] = useState("");
    const [pageNum,setPageNum]=useState(0);
    const [pageSize,setPageSize]=useState(10);
    const [loading,setLoading]=useState(true)
    const [edit,setEdit]=useState(false)

    let total = 0
    const config = {
      page_size: pageSize,
      length_menu: [10, 20, 50],
      show_filter: true,
      show_pagination: true,
      pagination: 'advance',
      language: {
        loading_text: "loading..."
    },
      button: {
          excel: false,
          csv: false,
          print:false
      }
  }
  const tableChangeHandler= data => {
    console.log(data)
    setPageNum(data.page_number-1)
    setPageSize(data.page_size)


  
}
    const handleShow = ()=>{ 
        setShow(true);
        setTitle(`Add New Category`);
        SetName('')
        setMessage('');
    }
    if(store !== undefined){

        console.log(store);
        // if(!store.auth.isLoggedIn){
        //     history.push("/login");
        //     window.location.reload();
        // }
    }
    const editCategory = (category)=>{ 
        setEdit(true)
        setShow(true);
        setTitle(`Edit ${category.name}`);
        SetName(category.name)
        setMessage('');

          
    }
    const handleDeleteKey = (category)=>{ 
            // alert(category.id)
            dispatch(
                deleteCategory(localStorage.getItem('token'),category.id)
              );
    }
  
  
    const handleHide = () => setShow(false);
    const [name,SetName] = useState("")
    const [nameAr,SetNameAr] = useState("")

    const handleSubmit = (e) => {
        console.log(store)
        // setShow(false);
        e.preventDefault();
        if(edit==false){
            dispatch(
                addCategory(localStorage.getItem('token'),name,nameAr)
              );
              setMessage('Success save category');
        }else{
            dispatch(
                updateCategory(localStorage.getItem('token'),name,nameAr)
              );
              setMessage('Success update category');
        }
      
      
      };
  

  
   
    const columns = [
        {
          text: 'Name',
          key: 'name',
          sortable: true,
        },
        {
            text: 'Name arabic',
            key: 'nameAr',
            sortable: true,
          },
        {
            text: "Actions",         
            key: "",
            className: "action",
            width: 100,
            align: "left",
            sortable: false,
            cell: record => {
                if (record.Action) {
                    return (
                        <Fragment>                                
                                {/* <button
                                    onClick={() => {
                                        editCategory(record.Action);
                                    }}
                                    type="submit"
                                    style={{padding:"3px",margin:"3px"}}
                                >
                                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-pencil-fill" viewBox="0 0 16 16">
  <path d="M12.854.146a.5.5 0 0 0-.707 0L10.5 1.793 14.207 5.5l1.647-1.646a.5.5 0 0 0 0-.708l-3-3zm.646 6.061L9.793 2.5 3.293 9H3.5a.5.5 0 0 1 .5.5v.5h.5a.5.5 0 0 1 .5.5v.5h.5a.5.5 0 0 1 .5.5v.5h.5a.5.5 0 0 1 .5.5v.207l6.5-6.5zm-7.468 7.468A.5.5 0 0 1 6 13.5V13h-.5a.5.5 0 0 1-.5-.5V12h-.5a.5.5 0 0 1-.5-.5V11h-.5a.5.5 0 0 1-.5-.5V10h-.5a.499.499 0 0 1-.175-.032l-.179.178a.5.5 0 0 0-.11.168l-2 5a.5.5 0 0 0 .65.65l5-2a.5.5 0 0 0 .168-.11l.178-.178z"/>
</svg>
                                   

                                </button> */}
                                
                                <button
                                    onClick={() => {
                                        handleDeleteKey(record.Action);
                                    }}
                                    type="submit"
                                    className="btn-danger" style={{padding:"3px",margin:"3px"}}
                                >
                                                                        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-trash" viewBox="0 0 16 16">
  <path d="M5.5 5.5A.5.5 0 0 1 6 6v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm2.5 0a.5.5 0 0 1 .5.5v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm3 .5a.5.5 0 0 0-1 0v6a.5.5 0 0 0 1 0V6z"/>
  <path fill-rule="evenodd" d="M14.5 3a1 1 0 0 1-1 1H13v9a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2V4h-.5a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1H6a1 1 0 0 1 1-1h2a1 1 0 0 1 1 1h3.5a1 1 0 0 1 1 1v1zM4.118 4 4 4.059V13a1 1 0 0 0 1 1h6a1 1 0 0 0 1-1V4.059L11.882 4H4.118zM2.5 3V2h11v1h-11z"/>
</svg>
                                </button>
                            </Fragment>
                    );
                }
            }
        }
      ];
    useEffect(() => {   
        // console.log(getInfo()); 
      dispatch(listCategory(localStorage.getItem('token'),pageNum))
      setTimeout(() => {
        setLoading(false)
    }, 1000);

    }, [pageNum,pageSize,loading])
    let element = [];       
    // console.log(store.eshailx.categories.data);
        if(store.eshailx.categories.data !== undefined){
            total = store.eshailx.categories.data.count;

            store.eshailx.categories.data.rows.map(function(item, i){
                element.push({'name':item.name,'nameAr':item.nameAr, 'Action':item})
                // element.push(<tr>
                //     <td>{item.name}</td>                                        
                //     <td class="text-center">
                //         <div class="dropdown">
                //             <a class="dropdown-toggle" href="#" role="button" id={"dropdownMenuLink"+(i+2)} data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                //                 <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-more-horizontal"><circle cx="12" cy="12" r="1"></circle><circle cx="19" cy="12" r="1"></circle><circle cx="5" cy="12" r="1"></circle></svg>
                //             </a>
                //             <div class="dropdown-menu" aria-labelledby={"dropdownMenuLink"+(i+2)}>
                //                 <a class="dropdown-item" href="#">View</a>
                //                 <a class="dropdown-item" href="#">Edit</a>
                //                 <a class="dropdown-item" href="#">Delete</a>
                //             </div>
                //         </div>
                //     </td>
                // </tr>);
            })
        }
    
        

return (
    <div>
                    {/* <div id="load_screen"> 
                            <div class="loader"> 
                                <div class="loader-content">
                                    <div class="spinner-grow align-self-center"></div>
                                </div>
                            </div>
                    </div>
                     */}
                   
        <Header />
        <Navbar />    
    
    <div class="main-container" id="container">
       
        <div class="overlay"></div>
        <div class="search-overlay"></div>
            <Sidebar />       
            
            <div id="content" class="main-content">
                <div class="layout-px-spacing">      
                        <div class="row layout-top-spacing">      

    <div class="col-xl-12 col-lg-12 col-sm-12  layout-spacing">
    <Button variant="success" onClick={handleShow}>New Category<div /></Button>

    <div class="widget-content widget-content-area br-6">   
  
        
        <Modal show={show}>
        <Modal.Header>
          <Modal.Title>{title}</Modal.Title>
        </Modal.Header>
        <Modal.Body>
        {message && (
              <div className="form-group">
                <div className="alert alert-success" role="alert">
                  {message}
                </div>
              </div>
            )}
        <Form onSubmit={(e)=>handleSubmit(e)}>   
                       
                            <div class="form">
                                <div id="name-field" class="field-wrapper input">
                                <div class="d-flex justify-content-between">
                                    <label for="name">Name</label>
                                </div>                                    
                                    <Input type="text" class="form-control" placeholder=""                                         
                                         name="name"
                                         id = "name"
                                         value={name}  
                                         required="required"                                                                               
                                         onChange={(e) => SetName(e.target.value)}                                       
                                    />
                                     <div class="d-flex justify-content-between">
                                    <label for="name">Name arabic</label>
                                </div> 
                                    <Input type="text" class="form-control" placeholder=""                                         
                                         name="nameAr"
                                         id = "nameAr"
                                         value={nameAr}  
                                         required="required"                                                                               
                                         onChange={(e) => SetNameAr(e.target.value)}                                       
                                    />
                                </div>                                                          
                            </div>   
                            <button class="btn btn-primary submit-fn mt-2" type="submit">Save</button>
 
                            </Form>                    
                        <div className="form-group">
                      
            </div>

            
            
                     
                     
        </Modal.Body>
        <Modal.Footer>
          <Button variant="btn btn submit-fn mt-2" onClick={handleHide}>Close</Button>
        </Modal.Footer>
      </Modal>
     
      <ReactDatatable
                config={config}
                records={element}
                columns={columns}
                dynamic={false}
                total_record={total}
                loading={loading}

                onChange={tableChangeHandler} />
      

     {/* <DataTable
        title="Categories"
        striped="True"
        columns={columns}
        data={element}
        pagination
        highlightOnHover
      />  */}
   

        {/* <table class="multi-table table table-striped table-bordered table-hover non-hover" >
            <thead>
                <tr>
                    <th>Name</th>                                   
                    <th class="text-center dt-no-sorting">Action</th>
                </tr>
            </thead>
            <tbody> 
            
                        {element}

              
            </tbody>
           
        </table> */}
    </div>
    
</div>
</div>            
                </div>            
            <Footer />
        </div>
    </div>
    </div>
);
                    }

export default Category;