import React from "react";



const Footer = () => (
    <div class="footer-wrapper">
    <div class="footer-section f-section-1">
        <p class="">Copyright © 2022 <a target="_blank" rel="noreferrer" href="https://eshailx.com/">Eshailx</a>, All rights reserved.</p>
    </div>
  
</div>
);


export default Footer;