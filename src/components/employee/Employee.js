import React,{useEffect,Fragment,useState} from "react";
import Header from '../common/Header'
import Navbar from '../common/Navbar'
import Footer from  '../common/Footer'
import Sidebar from '../common/Sidebar';
import { useSelector, useDispatch } from 'react-redux'
import DataTable from 'react-data-table-component';
import { Container, Row, Col } from "react-bootstrap";
import ReactDatatable from '@mkikets/react-datatable';
import Form from "react-validation/build/form";
import Input from "react-validation/build/input";
import { useParams } from "react-router-dom";

import "./style.css";
// import { ArrowRight } from 'react-bootstrap-icons';
import { listMerchantbranches,addEmployee } from '../../store/actions'
import { createBrowserHistory } from 'history'
import { Button, Modal } from "react-bootstrap";
function Employee (){
  const dispatch = useDispatch()
  const store = useSelector(state => state) 

    const history = createBrowserHistory()
    const [show, setShow] = useState(false);    
    const [message,setMessage] = useState("");
    const [alert,setAlert] = useState("");

    const [title,setTitle] = useState("");
    const [fName,setFName] = useState("");
    const [mName,setMName] = useState("");
    const [lName,setLName] = useState("");
    const [mobileNum,setMobileNum] = useState("");
    const [branchId,setBranchId] = useState("");
    const [rule,setRule] = useState("");

    const [pageNum,setPageNum]=useState(0);
    const [pageSize,setPageSize]=useState(10);
    const [loading,setLoading]=useState(true)
    let total = 0
    const config = {
      page_size: pageSize,
      length_menu: [10, 20, 50],
      show_filter: true,
      show_pagination: true,
      pagination: 'advance',
      language: {
        loading_text: "loading..."
    },
      button: {
          excel: false,
          csv: false,
          print:false
      }
  }

  const tableChangeHandler= data => {
      // console.log(data)
      setPageNum(data.page_number-1)
      setPageSize(data.page_size)
  }


    const handleShow = ()=>{ 
    //    history.push("/merchants");
    //    window.location.reload();
        setShow(true);
        setTitle(`Add New Employee`);
        setFName('')
        setMessage('');
    }
  
    const handleHide = () => setShow(false);
    const columns = [
        {
          text: 'First name',
          key: 'fName',
          sortable: true,
          cell: record => {
          return  <a href={`/merchantdetails/${record.Action.id}`} > <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-eye" viewBox="0 0 16 16">
          <path d="M16 8s-3-5.5-8-5.5S0 8 0 8s3 5.5 8 5.5S16 8 16 8zM1.173 8a13.133 13.133 0 0 1 1.66-2.043C4.12 4.668 5.88 3.5 8 3.5c2.12 0 3.879 1.168 5.168 2.457A13.133 13.133 0 0 1 14.828 8c-.058.087-.122.183-.195.288-.335.48-.83 1.12-1.465 1.755C11.879 11.332 10.119 12.5 8 12.5c-2.12 0-3.879-1.168-5.168-2.457A13.134 13.134 0 0 1 1.172 8z"/>
          <path d="M8 5.5a2.5 2.5 0 1 0 0 5 2.5 2.5 0 0 0 0-5zM4.5 8a3.5 3.5 0 1 1 7 0 3.5 3.5 0 0 1-7 0z"/>
        </svg> {record.Action.name}</a>
        


          }
        },
        {
          text: 'Middle name',
          key: 'mName',
          sortable: true,
        },
        {
          text: 'Last name',
          key: 'lName',
          sortable: true,
        },
        {
          text: 'Mobile',
          key: 'mobileNum',
          sortable: true,
        },
        {
            text: 'Branch name',
            key: 'branchId',
            sortable: true,
          },
          {
            text: 'Rule',
            key: 'rule',
            sortable: true,
          },
       
        {
            text: "Actions",         
            key: "action",
            className: "action",
            width: 100,
            align: "left",
            sortable: false,
            cell: record => {
                if (record.Action) {
                 
                    return (
                        <Fragment>   
                                              
                           
                           
                                { <button
                                    onClick={() => {
                                        // editCategory(record.Action);
                                    }}
                                    type="submit"
                                    style={{padding:"3px",margin:"3px"}}
                                >
                                  <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-pencil-fill" viewBox="0 0 16 16">
                                  <path d="M12.854.146a.5.5 0 0 0-.707 0L10.5 1.793 14.207 5.5l1.647-1.646a.5.5 0 0 0 0-.708l-3-3zm.646 6.061L9.793 2.5 3.293 9H3.5a.5.5 0 0 1 .5.5v.5h.5a.5.5 0 0 1 .5.5v.5h.5a.5.5 0 0 1 .5.5v.5h.5a.5.5 0 0 1 .5.5v.207l6.5-6.5zm-7.468 7.468A.5.5 0 0 1 6 13.5V13h-.5a.5.5 0 0 1-.5-.5V12h-.5a.5.5 0 0 1-.5-.5V11h-.5a.5.5 0 0 1-.5-.5V10h-.5a.499.499 0 0 1-.175-.032l-.179.178a.5.5 0 0 0-.11.168l-2 5a.5.5 0 0 0 .65.65l5-2a.5.5 0 0 0 .168-.11l.178-.178z"/>
                                  </svg>


                                </button>
                             }
                            </Fragment>
                    );
                }
            }
        }
      ];
      const handleSubmit = (e) => {
          console.log('submit button');
          // setShow(false);
          e.preventDefault();
          dispatch(
            addEmployee(localStorage.getItem('token'),id,fName,mName ,lName,mobileNum,branchId,rule)
            );
            console.log(store)

            // if(store.message !== undefined && store.message!=='' && store.message.message!==undefined ){
            //   console.log('hi')
            //   setAlert('danger')
            //   console.log(store.message.message)
            //   setMessage(store.message.message);
            // }else{
            //   setAlert('success')
            //   setMessage('Success add branch');

            // }
            setAlert('success')
            setMessage('Success add branch');
        
        };
        const { id } =  useParams();

    useEffect(() => {   
      dispatch(listMerchantbranches(localStorage.getItem('token'),id,pageNum))
      setTimeout(() => {
        // this.setState({
        //     loading: false,
        //     records: records
        // })
        setLoading(false)
    }, 1000);


    }, [pageNum,pageSize,loading,id])
    let element = []; 
    if(store.eshailx.merchant_branch_list.data !== undefined){
      store.eshailx.merchant_branch_list.data.rows.map(function(item, i){
          element.push({'name':item.branchName,'phone':item.phone,'lat':item.lat,'long':item.long,'address':item.address,'Action':item})
         
      })
  }

    
return (
    <div>
                    {/* <div id="load_screen"> 
                            <div class="loader"> 
                                <div class="loader-content">
                                    <div class="spinner-grow align-self-center"></div>
                                </div>
                            </div>
                    </div>
                     */}
                   
        <Header />
        <Navbar />    
    
    <div class="main-container" id="container">
       
        <div class="overlay"></div>
        <div class="search-overlay"></div>
            <Sidebar />       
            
            <div id="content" class="main-content">
                <div class="layout-px-spacing">      
                        <div class="row layout-top-spacing">      

    <div class="col-xl-12 col-lg-12 col-sm-12  layout-spacing">
    <Button variant="success" onClick={handleShow}> New Employee<div /></Button>

    <div class="widget-content widget-content-area br-6">   
  
        
        <Modal show={show}>
        <Modal.Header>
          <Modal.Title>{title}</Modal.Title>
        </Modal.Header>
        <Modal.Body>
        {message && (
              <div className="form-group">
                <div className={`alert alert-${alert}`} role="alert">
                  {message}
                </div>
              </div>
            )}

            <Form onSubmit={(e)=>handleSubmit(e)}>   
                       <div class="form">
                       <div id="name-field" class="field-wrapper input">
                               <div class="d-flex justify-content-between">
                                   <label for="name">First name</label>
                               </div>                                    
                                   <Input type="text" class="form-control" placeholder=""                                         
                                       name="fName"
                                       id = "fName"
                                       value={fName}  
                                       required="required"                                                                               
                                       onChange={(e) => setFName(e.target.value)}                                       
                                   />
                           </div> 
                          
                           <div id="name-field" class="field-wrapper input">
                               <div class="d-flex justify-content-between">
                                   <label for="name">Middle name</label>
                               </div>                                    
                                   <Input type="text" class="form-control" placeholder=""                                         
                                       name="mName"
                                       id = "mName"
                                       value={mName}  
                                       required="required"                                                                               
                                       onChange={(e) => setMName(e.target.value)}                                       
                                   />
                           </div> 
                           <div id="name-field" class="field-wrapper input">
                               <div class="d-flex justify-content-between">
                                   <label for="name">Last name</label>
                               </div>                                    
                                   <Input type="text" class="form-control" placeholder=""                                         
                                       name="lName"
                                       id = "lName"
                                       value={lName}  
                                       required="required"                                                                               
                                       onChange={(e) => setLName(e.target.value)}                                       
                                   />
                           </div> 
                           <div id="name-field" class="field-wrapper input">
                               <div class="d-flex justify-content-between">
                                   <label for="name">Mobile</label>
                               </div>                                    
                                   <Input type="text" class="form-control" placeholder=""                                         
                                       name="mobileNum"
                                       id = "mobileNum"
                                       value={mobileNum}  
                                       required="required"                                                                               
                                       onChange={(e) => setMobileNum(e.target.value)}                                       
                                   />
                           </div> 
                           
                           <div id="name-field" class="field-wrapper input">
                               <div class="d-flex justify-content-between">
                                   <label for="name">Branch name</label>
                               </div>                                    
                                   <Input type="text" class="form-control" placeholder=""                                         
                                       name="branchId"
                                       id = "branchId"
                                       value={branchId}  
                                       required="required"                                                                               
                                       onChange={(e) => setBranchId(e.target.value)}                                       
                                   />
                           </div> 
                           <div id="name-field" class="field-wrapper input">
                               <div class="d-flex justify-content-between">
                                   <label for="name">Rule</label>
                               </div>                                    
                                   <Input type="text" class="form-control" placeholder=""                                         
                                       name="rule"
                                       id = "rule"
                                       value={rule}  
                                       required="required"                                                                               
                                       onChange={(e) => setRule(e.target.value)}                                       
                                   />
                           </div> 
                       </div>   
                       <button class="btn btn-primary submit-fn mt-2" type="submit">Save</button>

   </Form>    
                           
                     
        </Modal.Body>
        <Modal.Footer>
          <Button variant="btn btn submit-fn mt-2" onClick={handleHide}>Close</Button>
        </Modal.Footer>
      </Modal>
      <ReactDatatable
                config={config}
                records={element}
                columns={columns}
                dynamic={true}
                total_record={total}
                loading={loading}

                onChange={tableChangeHandler}
                />
    </div>
    
</div>
</div>            
                </div>            
            <Footer />
        </div>
    </div>
    </div>
);
                    }

 export default Employee;
