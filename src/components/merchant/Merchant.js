import React,{useEffect,Fragment,useState} from "react";
import Header from '../common/Header'
import Navbar from '../common/Navbar'
import Footer from  '../common/Footer'
import Sidebar from '../common/Sidebar';
import { useSelector, useDispatch } from 'react-redux'
import DataTable from 'react-data-table-component';
import { Container, Row, Col } from "react-bootstrap";
import ReactDatatable from '@mkikets/react-datatable';
import { NavLink } from "react-router-dom";

import "./style.css";
// import { ArrowRight } from 'react-bootstrap-icons';

// import SuccessPopup from 'react-success-popup'

import { listMerchant,approveMerchant } from '../../store/actions'

import { createBrowserHistory } from 'history'


// import data from '../data.json';
import { Button, Modal } from "react-bootstrap";
import StepOne  from "../StepOne";
import StepTwo  from "../StepTwo";
import StepFinal  from "../StepFinal";
// import Multistep from 'react-multistep'

function Merchant (){
  const dispatch = useDispatch()
  const store = useSelector(state => state) 

    const history = createBrowserHistory()
    const [show, setShow] = useState(false);    
    const [message,setMessage] = useState("");
    const [title,setTitle] = useState("");
    const [pageNum,setPageNum]=useState(0);
    const [pageSize,setPageSize]=useState(10);
    const [loading,setLoading]=useState(true)
    let total = 0
    const config = {
      page_size: pageSize,
      length_menu: [10, 20, 50],
      show_filter: true,
      show_pagination: true,
      pagination: 'advance',
      language: {
        loading_text: "loading..."
    },
      button: {
          excel: false,
          csv: false,
          print:false
      }
  }

  const tableChangeHandler= data => {
      console.log(data)

      setPageNum(data.page_number-1)
      setPageSize(data.page_size)


    
  }
 
    // const steps = [
    //     {name: 'StepOne', component: <StepOne/>},
    //     {name: 'StepTwo', component: <StepTwo/>},
    //     {name: 'StepThree', component: <StepThree/>},
    //     {name: 'StepFour', component: <StepFour/>}
    //   ];
// custom styles
const prevStyle = { background: '#ffffff' }
const nextStyle = { background: '#ffffff' }
const [step, setstep] = useState(1);
//state for form data
const [formData, setFormData] = useState({
  mobile: "",
  password: "",
  name: "",
  hotLine:"",
  userCurrncy: "",
  pointPrice:"",
  currency:"",
  commissionPercentage:"",
  taxRegistrationNumber:"",
  categoryId:""

})

// function for going to next step by increasing step state by 1
const nextStep = () => {
  setstep(step + 1);
};

// function for going to previous step by decreasing step state by 1
const prevStep = () => {
  setstep(step - 1);
};

// handling form input data by taking onchange value and updating our previous form data state
const handleInputData = input => e => {
  // input value from the form
  const {value } = e.target;

  //updating for data state taking previous state and then adding new value to create new object
  setFormData(prevState => ({
    ...prevState,
    [input]: value
}));
}
    const handleShow = ()=>{ 
    //    history.push("/merchants");
    //    window.location.reload();
        setShow(true);
        setTitle(`Add New Merchant`);
        SetName('')
        setMessage('');
    }
  
    const approved = (merchant)=>{ 
      // alert(category.id)
      dispatch(
          approveMerchant(localStorage.getItem('token'),merchant.MerchantProfile.id)
        );
        alert('Approved success');
}


    const handleHide = () => setShow(false);
    const [name,SetName] = useState("")
    // const handleSubmit = (e) => {
    //     console.log('submit button');
    //     console.log(store)
    //     // setShow(false);
    //     e.preventDefault();
    //     dispatch(
    //         addCategory(name)
    //       );
    //       setMessage('Success save category');
      
    //   };
  

  


    const columns = [
        {
          text: 'Name',
          key: 'name',
          sortable: true,
          cell: record => {
          return <NavLink  class="nav-link dropdown-toggle" to={`/merchantdetails/${record.Action.id}`}>
             <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-eye" viewBox="0 0 16 16">
          <path d="M16 8s-3-5.5-8-5.5S0 8 0 8s3 5.5 8 5.5S16 8 16 8zM1.173 8a13.133 13.133 0 0 1 1.66-2.043C4.12 4.668 5.88 3.5 8 3.5c2.12 0 3.879 1.168 5.168 2.457A13.133 13.133 0 0 1 14.828 8c-.058.087-.122.183-.195.288-.335.48-.83 1.12-1.465 1.755C11.879 11.332 10.119 12.5 8 12.5c-2.12 0-3.879-1.168-5.168-2.457A13.134 13.134 0 0 1 1.172 8z"/>
          <path d="M8 5.5a2.5 2.5 0 1 0 0 5 2.5 2.5 0 0 0 0-5zM4.5 8a3.5 3.5 0 1 1 7 0 3.5 3.5 0 0 1-7 0z"/>
        </svg>
           {record.Action.name}</NavLink>


          }
        },
        {
          text: 'Mobile',
          key: 'mobile',
          sortable: true,
        },
        {
          text: 'User Currncy',
          key: 'userCurrncy',
          sortable: true,
        },
        {
          text: 'Balance',
          key: 'balance',
          sortable: true,
        },
        {
          text: 'Approved',
          key: 'isAproved',
          sortable: true
        },
        {
          text: 'Live',
          key: 'isLive',
          sortable: true,
        },
        {
            text: "Actions",         
            key: "action",
            className: "action",
            width: 100,
            align: "left",
            sortable: false,
            cell: record => {
                if (record.Action) {
                  let elmappoved = ''
                  if(record.Action.MerchantProfile!==null && record.Action.MerchantProfile.id!=undefined){
                  // console.log(record.Action.MerchantProfile.id)
               if(record.Action.isAproved===false){
                      elmappoved = <button
                      onClick={() => {
                        approved(record.Action);
                      }}
                        type="submit"
                        style={{padding:"3px",margin:"3px"}}>
                        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-person-x-fill" viewBox="0 0 16 16">
                        <path fill-rule="evenodd" d="M1 14s-1 0-1-1 1-4 6-4 6 3 6 4-1 1-1 1H1zm5-6a3 3 0 1 0 0-6 3 3 0 0 0 0 6zm6.146-2.854a.5.5 0 0 1 .708 0L14 6.293l1.146-1.147a.5.5 0 0 1 .708.708L14.707 7l1.147 1.146a.5.5 0 0 1-.708.708L14 7.707l-1.146 1.147a.5.5 0 0 1-.708-.708L13.293 7l-1.147-1.146a.5.5 0 0 1 0-.708z"/>
                      </svg>
                  </button>
               }else{
                elmappoved =
                <div
                style={{padding:"3px",margin:"3px"}}>
                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-person-check" viewBox="0 0 16 16">
                <path d="M6 8a3 3 0 1 0 0-6 3 3 0 0 0 0 6zm2-3a2 2 0 1 1-4 0 2 2 0 0 1 4 0zm4 8c0 1-1 1-1 1H1s-1 0-1-1 1-4 6-4 6 3 6 4zm-1-.004c-.001-.246-.154-.986-.832-1.664C9.516 10.68 8.289 10 6 10c-2.29 0-3.516.68-4.168 1.332-.678.678-.83 1.418-.832 1.664h10z"/>
                <path fill-rule="evenodd" d="M15.854 5.146a.5.5 0 0 1 0 .708l-3 3a.5.5 0 0 1-.708 0l-1.5-1.5a.5.5 0 0 1 .708-.708L12.5 7.793l2.646-2.647a.5.5 0 0 1 .708 0z"/>
                </svg>
                </div>
               }
               
                  
                  }
                    return (
                        <Fragment>   
                                              
                            {elmappoved}
                           
                           
                                {/* { <button
                                    onClick={() => {
                                        // editCategory(record.Action);
                                    }}
                                    type="submit"
                                    style={{padding:"3px",margin:"3px"}}
                                >
                                  <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-pencil-fill" viewBox="0 0 16 16">
                                  <path d="M12.854.146a.5.5 0 0 0-.707 0L10.5 1.793 14.207 5.5l1.647-1.646a.5.5 0 0 0 0-.708l-3-3zm.646 6.061L9.793 2.5 3.293 9H3.5a.5.5 0 0 1 .5.5v.5h.5a.5.5 0 0 1 .5.5v.5h.5a.5.5 0 0 1 .5.5v.5h.5a.5.5 0 0 1 .5.5v.207l6.5-6.5zm-7.468 7.468A.5.5 0 0 1 6 13.5V13h-.5a.5.5 0 0 1-.5-.5V12h-.5a.5.5 0 0 1-.5-.5V11h-.5a.5.5 0 0 1-.5-.5V10h-.5a.499.499 0 0 1-.175-.032l-.179.178a.5.5 0 0 0-.11.168l-2 5a.5.5 0 0 0 .65.65l5-2a.5.5 0 0 0 .168-.11l.178-.178z"/>
                                  </svg>


                                </button>
                             } */}
                            </Fragment>
                    );
                }
            }
        }
      ];
      
    useEffect(() => {   
        // console.log(getInfo()); 
      dispatch(listMerchant(localStorage.getItem('token'),pageNum))

      setTimeout(() => {
        // this.setState({
        //     loading: false,
        //     records: records
        // })
        setLoading(false)
    }, 1000);


    }, [pageNum,pageSize,loading])
    let element = []; 
    let elem = '';
    // console.log('step')      
    // console.log(store)
    // console.log(store.eshailx.merchants.data);
    // console.log(store.eshailx.merchants.data.count)

        if(store.eshailx.merchants.data !== undefined){
          // if( store.eshailx.merchants.data.count !==undefined){
            total = store.eshailx.merchants.data.count;
          // }
       
            store.eshailx.merchants.data.rows.map(function(item, i){
            let isAproved="No";
            let isLive = 'No';
              if(item.isAproved===true){
                isAproved = "Yes";
            }
            if(item.isLive===true){
              isLive = 'Yes';
          }
                element.push({'name':item.name,'mobile':item.mobile,'userCurrncy':item.userCurrncy,'balance':item.balance,'isAproved':isAproved,'isLive':isLive, 'Action':item})
                // element.push(<tr>
                //     <td>{item.name}</td>                                        
                //     <td class="text-center">
                //         <div class="dropdown">
                //             <a class="dropdown-toggle" href="#" role="button" id={"dropdownMenuLink"+(i+2)} data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                //                 <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-more-horizontal"><circle cx="12" cy="12" r="1"></circle><circle cx="19" cy="12" r="1"></circle><circle cx="5" cy="12" r="1"></circle></svg>
                //             </a>
                //             <div class="dropdown-menu" aria-labelledby={"dropdownMenuLink"+(i+2)}>
                //                 <a class="dropdown-item" href="#">View</a>
                //                 <a class="dropdown-item" href="#">Edit</a>
                //                 <a class="dropdown-item" href="#">Delete</a>
                //             </div>
                //         </div>
                //     </td>
                // </tr>);
            })
        }
    
        
      
            // case 1 to show stepOne form and passing nextStep, prevStep, and handleInputData as handleFormData method as prop and also formData as value to the fprm
          if(step === 1){
            elem =  
            <div className="App">
              <Container>
                <Row>
                  <Col>
                    <StepOne nextStep={nextStep} handleFormData={handleInputData} values={formData} />
                  </Col>
                </Row>
              </Container>
            </div>;
          }else if(step === 2){
                elem =  
                <div className="App">
                  <Container>
                    <Row>
                      <Col>
                        <StepTwo nextStep={nextStep} prevStep={prevStep} handleFormData={handleInputData} values={formData} />
                      </Col>
                    </Row>
                  </Container>
                </div>;
              
          }else{
                elem =  
                <div className="App">
                  <Container>
                    <Row>
                      <Col>
                        <StepFinal values={formData}  />
                      </Col>
                    </Row>
                  </Container>
                </div>;
              
            // default case to show nothing
        
          }
return (
    <div>
                    {/* <div id="load_screen"> 
                            <div class="loader"> 
                                <div class="loader-content">
                                    <div class="spinner-grow align-self-center"></div>
                                </div>
                            </div>
                    </div>
                     */}
                   
        <Header />
        <Navbar />    
    
    <div class="main-container" id="container">
       
        <div class="overlay"></div>
        <div class="search-overlay"></div>
            <Sidebar />       
            
            <div id="content" class="main-content">
                <div class="layout-px-spacing">      
                        <div class="row layout-top-spacing">      

    <div class="col-xl-12 col-lg-12 col-sm-12  layout-spacing">
    <Button variant="success" onClick={handleShow}> New Merchant<div /></Button>

    <div class="widget-content widget-content-area br-6">   
  
        
        <Modal show={show}>
        <Modal.Header>
          <Modal.Title>{title}</Modal.Title>
        </Modal.Header>
        <Modal.Body>
        {message && (
              <div className="form-group">
                <div className="alert alert-success" role="alert">
                  {message}
                </div>
              </div>
            )}

            
            {elem}
{/* <Multistep activeStep={0} showNavigation={true} steps={steps} prevStyle={prevStyle} nextStyle={nextStyle}/> */}

        {/* <Form onSubmit={(e)=>handleSubmit(e)}>   
                       
                            <div class="form">
                                <div id="name-field" class="field-wrapper input">
                                <div class="d-flex justify-content-between">
                                    <label for="name">Name</label>
                                    </div>                                    
                                    <Input type="text" class="form-control" placeholder=""                                         
                                         name="name"
                                         id = "name"
                                         value={name}  
                                         required="required"                                                                               
                                         onChange={(e) => SetName(e.target.value)}                                       
                                    />
                                </div>                                                          
                            </div>   
                            <button class="btn btn-primary submit-fn mt-2" type="submit">Save</button>
 
                            </Form>                    
                        <div className="form-group">
                      
            </div> */}

            
            
                     
                     
        </Modal.Body>
        <Modal.Footer>
          <Button variant="btn btn submit-fn mt-2" onClick={handleHide}>Close</Button>
        </Modal.Footer>
      </Modal>
     
 
      
      <ReactDatatable
                config={config}
                records={element}
                columns={columns}
                dynamic={true}
                total_record={total}
                loading={loading}

                onChange={tableChangeHandler}
                />
     {/* <DataTable
        title="Merchants"
        striped="True"
        columns={columns}
        data={element}
        pagination
        pagesAmount={4}
        highlightOnHover
      /> */}
   

        {/* <table class="multi-table table table-striped table-bordered table-hover non-hover" >
            <thead>
                <tr>
                    <th>Name</th>                                   
                    <th class="text-center dt-no-sorting">Action</th>
                </tr>
            </thead>
            <tbody> 
            
                        {element}

              
            </tbody>
           
        </table> */}
    </div>
    
</div>
</div>            
                </div>            
            <Footer />
        </div>
    </div>
    </div>
);
                    }

 export default Merchant;
