import React,{useEffect,Fragment,useState} from "react";
import Header from '../common/Header'
import Navbar from '../common/Navbar'
import Footer from  '../common/Footer'
import Sidebar from '../common/Sidebar';
import { useSelector, useDispatch } from 'react-redux'
// import DataTable from 'react-data-table-component';
import { Container, Row, Col } from "react-bootstrap";
import { useParams } from "react-router-dom";
import { Table, Card } from "react-bootstrap";
import ReactDatatable from '@mkikets/react-datatable';
import { createBrowserHistory } from 'history'
import { NavLink } from "react-router-dom";
import "./style.css";
// import { ArrowRight } from 'react-bootstrap-icons';

// import SuccessPopup from 'react-success-popup'

import { merchantDetails,listMerchantbranches,listMerchantemployee } from '../../store/actions'
import Form from "react-validation/build/form";
import Input from "react-validation/build/input";


// import data from '../data.json';
import { Button, Modal } from "react-bootstrap";
// import Multistep from 'react-multistep'

function MerchantDetails (){
  const dispatch = useDispatch()
  const store = useSelector(state => state)    
  const [show, setShow] = useState(false);    
    const [message,setMessage] = useState("");
    const [title,setTitle] = useState("");
    const history = createBrowserHistory()
    const [pageNum,setPageNum]=useState(0);
    const [pageSize,setPageSize]=useState(10);
    const [loading,setLoading]=useState(true)
    let totalBranches = 0
    let totalEmployee = 0
    const config = {
      page_size: pageSize,
      length_menu: [10, 20, 50],
      show_filter: true,
      show_pagination: true,
      pagination: 'advance',
      language: {
        loading_text: "loading..."
    },
      button: {
          excel: false,
          csv: false,
          print:false
      }
  }
  const tableChangeHandler= data => {
    console.log(data)
    setPageNum(data.page_number-1)
    setPageSize(data.page_size)


  
}
    let element_top_branch_html = []
    let element_top_employee_html = []

    const handleHide = () => setShow(false);
    // const [name,SetName] = useState("")

    const { id } =  useParams();
    let { mobile, name, isAproved,isLive,balance,userCurrncy,hotLine,pointPrice,currency,commissionPercentage,taxRegistrationNumber,categoryId } = '';

    useEffect(() => {   
        console.log(id)
        // console.log(getInfo()); 
      dispatch(merchantDetails(localStorage.getItem('token'),id))
      dispatch(listMerchantbranches(localStorage.getItem('token'),id))
      dispatch(listMerchantemployee(localStorage.getItem('token'),id))

      setTimeout(() => {
        setLoading(false)
    }, 1000);

    }, [pageNum,pageSize,loading,id])
    let element_top_branch = []; 
    let element_top_employee = []; 
    console.log('step')      
    console.log(store)
   
    const showBranchDetails = ()=>{
        history.push("/login");
        window.location.reload();
    }
    const handleShow = ()=>{ 
        //    history.push("/merchants");
        //    window.location.reload();
            setShow(true);
            setTitle(`Add New Branch`);
            // SetName('')
            setMessage('');
        }
  
        const columnsBranches = [
            {
              text: 'Name',
              key: 'branchName',
              sortable: true,
            },
            {
              text: 'phone',
              key: 'phone',
              sortable: true,
            },
            {
              text: 'latitude',
              key: 'lat',
              sortable: true,
            },
            {
              text: 'longitude',
              key: 'long',
              sortable: true,
            },
           
            {
                text: "Actions",         
                key: "action",
                className: "action",
                width: 100,
                align: "left",
                sortable: false,
                cell: record => {
                    if (record.Action) {
                     
                        return (
                            <Fragment>   
                                                  
                               
                               
                                    {/* { <button
                                        onClick={() => {
                                            // editCategory(record.Action);
                                        }}
                                        type="submit"
                                        style={{padding:"3px",margin:"3px"}}
                                    > */}
                                     {/* <a href={`/merchantdetails/${record.Action.id}`} ></a>
                                      <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-pencil-fill" viewBox="0 0 16 16">
                                      <path d="M12.854.146a.5.5 0 0 0-.707 0L10.5 1.793 14.207 5.5l1.647-1.646a.5.5 0 0 0 0-.708l-3-3zm.646 6.061L9.793 2.5 3.293 9H3.5a.5.5 0 0 1 .5.5v.5h.5a.5.5 0 0 1 .5.5v.5h.5a.5.5 0 0 1 .5.5v.5h.5a.5.5 0 0 1 .5.5v.207l6.5-6.5zm-7.468 7.468A.5.5 0 0 1 6 13.5V13h-.5a.5.5 0 0 1-.5-.5V12h-.5a.5.5 0 0 1-.5-.5V11h-.5a.5.5 0 0 1-.5-.5V10h-.5a.499.499 0 0 1-.175-.032l-.179.178a.5.5 0 0 0-.11.168l-2 5a.5.5 0 0 0 .65.65l5-2a.5.5 0 0 0 .168-.11l.178-.178z"/>
                                      </svg><a/> */}
                                      


    
{/*     
                                        </button>
                                     } */}

{ 
//                                      <a href={`/branchdetails/${record.Action.id}/${record.Action.merchantId}`} ><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-eye" viewBox="0 0 16 16">
// <path d="M16 8s-3-5.5-8-5.5S0 8 0 8s3 5.5 8 5.5S16 8 16 8zM1.173 8a13.133 13.133 0 0 1 1.66-2.043C4.12 4.668 5.88 3.5 8 3.5c2.12 0 3.879 1.168 5.168 2.457A13.133 13.133 0 0 1 14.828 8c-.058.087-.122.183-.195.288-.335.48-.83 1.12-1.465 1.755C11.879 11.332 10.119 12.5 8 12.5c-2.12 0-3.879-1.168-5.168-2.457A13.134 13.134 0 0 1 1.172 8z"/>
// <path d="M8 5.5a2.5 2.5 0 1 0 0 5 2.5 2.5 0 0 0 0-5zM4.5 8a3.5 3.5 0 1 1 7 0 3.5 3.5 0 0 1-7 0z"/>
// </svg></a>


                                      


    
    
                                        
                                     }






                                 
                                </Fragment>
                        );
                    }
                }
            }
          ];
          const Employeecolumns = [
            {
              text: 'First name',
              key: 'fName',
              sortable: true,
              cell: record => {
              return  <a href={`/merchantdetails/${record.Action.id}`} > <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-eye" viewBox="0 0 16 16">
              <path d="M16 8s-3-5.5-8-5.5S0 8 0 8s3 5.5 8 5.5S16 8 16 8zM1.173 8a13.133 13.133 0 0 1 1.66-2.043C4.12 4.668 5.88 3.5 8 3.5c2.12 0 3.879 1.168 5.168 2.457A13.133 13.133 0 0 1 14.828 8c-.058.087-.122.183-.195.288-.335.48-.83 1.12-1.465 1.755C11.879 11.332 10.119 12.5 8 12.5c-2.12 0-3.879-1.168-5.168-2.457A13.134 13.134 0 0 1 1.172 8z"/>
              <path d="M8 5.5a2.5 2.5 0 1 0 0 5 2.5 2.5 0 0 0 0-5zM4.5 8a3.5 3.5 0 1 1 7 0 3.5 3.5 0 0 1-7 0z"/>
            </svg> {record.Action.name}</a>
            
    
    
              }
            },
            {
              text: 'Middle name',
              key: 'mName',
              sortable: true,
            },
            {
              text: 'Last name',
              key: 'lName',
              sortable: true,
            },
            {
              text: 'Mobile',
              key: 'mobileNum',
              sortable: true,
            },
            {
                text: 'Branch name',
                key: 'branchId',
                sortable: true,
              },
              {
                text: 'Rule',
                key: 'rule',
                sortable: true,
              },
           
            {
                text: "Actions",         
                key: "action",
                className: "action",
                width: 100,
                align: "left",
                sortable: false,
                cell: record => {
                    if (record.Action) {
                     
                        return (
                            <Fragment>   
                                                  
                               
                               
                                    { <button
                                        onClick={() => {
                                            // editCategory(record.Action);
                                        }}
                                        type="submit"
                                        style={{padding:"3px",margin:"3px"}}
                                    >
                                      <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-pencil-fill" viewBox="0 0 16 16">
                                      <path d="M12.854.146a.5.5 0 0 0-.707 0L10.5 1.793 14.207 5.5l1.647-1.646a.5.5 0 0 0 0-.708l-3-3zm.646 6.061L9.793 2.5 3.293 9H3.5a.5.5 0 0 1 .5.5v.5h.5a.5.5 0 0 1 .5.5v.5h.5a.5.5 0 0 1 .5.5v.5h.5a.5.5 0 0 1 .5.5v.207l6.5-6.5zm-7.468 7.468A.5.5 0 0 1 6 13.5V13h-.5a.5.5 0 0 1-.5-.5V12h-.5a.5.5 0 0 1-.5-.5V11h-.5a.5.5 0 0 1-.5-.5V10h-.5a.499.499 0 0 1-.175-.032l-.179.178a.5.5 0 0 0-.11.168l-2 5a.5.5 0 0 0 .65.65l5-2a.5.5 0 0 0 .168-.11l.178-.178z"/>
                                      </svg>
    
    
                                    </button>
                                 }
                                </Fragment>
                        );
                    }
                }
            }
          ];
      if(store.eshailx.merchant_details.data !==undefined){
        mobile = store.eshailx.merchant_details.data.mobile
        name = store.eshailx.merchant_details.data.name
        userCurrncy =  store.eshailx.merchant_details.data.userCurrncy
        
        if(store.eshailx.merchant_details.data.isAproved===false){
          isAproved  ='No'
        }else{
            isAproved = 'Yes' 
        }
        if(store.eshailx.merchant_details.data.isLive===false){
            isLive  ='No'
          }else{
            isLive = 'Yes' 
          }
       // isLive = store.eshailx.merchant_details.data.isLive
        balance = store.eshailx.merchant_details.data.balance
     

    }
    console.log('log')
    // console.log(store.eshailx.merchant_details.data.MerchantProfile)
    if(store.eshailx.merchant_details.data !==undefined && store.eshailx.merchant_details.data.MerchantProfile!=null){
     //    console.log(store.eshailx.merchant_details.data.MerchantProfile)

        hotLine = store.eshailx.merchant_details.data.MerchantProfile.hotLine
        pointPrice = store.eshailx.merchant_details.data.MerchantProfile.pointPrice
        currency = store.eshailx.merchant_details.data.MerchantProfile.currency
        commissionPercentage = store.eshailx.merchant_details.data.MerchantProfile.commissionPercentage
        taxRegistrationNumber = store.eshailx.merchant_details.data.MerchantProfile.taxRegistrationNumber
        categoryId = store.eshailx.merchant_details.data.MerchantProfile.categoryId


    }
    // console.log(store.eshailx.merchants.data);
        if(store.eshailx.merchant_branch_list.data !== undefined){
            totalBranches = store.eshailx.merchant_branch_list.data.count
            store.eshailx.merchant_branch_list.data.rows.map(function(item, i){
              //  element_top_branch.push({'branchName':item.branchName,'phone':item.phone,'lat':item.lat,'long':item.long,'address':item.address,'Action':item})
                element_top_branch_html.push(
                <tr>
                <td><div class="td-content"><span>{item.branchName}</span></div></td>
                <td><div class="td-content"><span>{item.phone}</span></div></td>
                <td><div class="td-content"><span>{item.lat}</span></div></td>
                <td><div class="td-content"><span>{item.long}</span></div></td>


            </tr>

                ) 
            })
        }
        if(store.eshailx.merchant_user_list.data !== undefined){
            totalEmployee = store.eshailx.merchant_user_list.data.count
            store.eshailx.merchant_user_list.data.rows.map(function(item, i){
               // element_top_employee.push({'Firstname':item.fName,'Middlename':item.mName,'Lastname':item.lName,'MobileNum':item.mobileNum,'rule':item.rule,'Action':item})
          
               element_top_employee_html.push(
                <tr>
                <td><div class="td-content"><span class="pricing">{item.fName}</span></div></td>
                <td><div class="td-content"><span class="discount-pricing">{item.mName}</span></div></td>
                <td><div class="td-content">{item.lName}</div></td>
                <td><div class="td-content">{item.mobileNum}</div></td>


            </tr> 
               )
            })
        }

        

return (
    <div>
                    {/* <div id="load_screen"> 
                            <div class="loader"> 
                                <div class="loader-content">
                                    <div class="spinner-grow align-self-center"></div>
                                </div>
                            </div>
                    </div>
                     */}
                   
        <Header />
        <Navbar />    
    
    <div class="main-container" id="container">
       
        <div class="overlay"></div>
        <div class="search-overlay"></div>
            <Sidebar />       
            
            <div id="content" class="main-content">
                <div class="layout-px-spacing">      
                        <div class="row layout-top-spacing">      

<>
<div class="col-lg-12 col-sm-12  layout-spacing">
        <Row>
                        <Col sm={4}>
                            <Card>
                                <Card.Body>
                                    <Card.Title><h3 className="text-center" style={{ color: '#4361EE' }}>
<NavLink style={{ color: '#4361EE' }}  class="nav-link" to={`/branches/${id}`}><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-shop" viewBox="0 0 16 16">
  <path d="M2.97 1.35A1 1 0 0 1 3.73 1h8.54a1 1 0 0 1 .76.35l2.609 3.044A1.5 1.5 0 0 1 16 5.37v.255a2.375 2.375 0 0 1-4.25 1.458A2.371 2.371 0 0 1 9.875 8 2.37 2.37 0 0 1 8 7.083 2.37 2.37 0 0 1 6.125 8a2.37 2.37 0 0 1-1.875-.917A2.375 2.375 0 0 1 0 5.625V5.37a1.5 1.5 0 0 1 .361-.976l2.61-3.045zm1.78 4.275a1.375 1.375 0 0 0 2.75 0 .5.5 0 0 1 1 0 1.375 1.375 0 0 0 2.75 0 .5.5 0 0 1 1 0 1.375 1.375 0 1 0 2.75 0V5.37a.5.5 0 0 0-.12-.325L12.27 2H3.73L1.12 5.045A.5.5 0 0 0 1 5.37v.255a1.375 1.375 0 0 0 2.75 0 .5.5 0 0 1 1 0zM1.5 8.5A.5.5 0 0 1 2 9v6h1v-5a1 1 0 0 1 1-1h3a1 1 0 0 1 1 1v5h6V9a.5.5 0 0 1 1 0v6h.5a.5.5 0 0 1 0 1H.5a.5.5 0 0 1 0-1H1V9a.5.5 0 0 1 .5-.5zM4 15h3v-5H4v5zm5-5a1 1 0 0 1 1-1h2a1 1 0 0 1 1 1v3a1 1 0 0 1-1 1h-2a1 1 0 0 1-1-1v-3zm3 0h-2v3h2v-3z"/>
</svg>
Branches</NavLink>

</h3></Card.Title>
                                    <Card.Text className="text-center">
                                    5
                                    </Card.Text>
                                    {/* <Card.Link href="#">Card Link</Card.Link> */}
                                </Card.Body>
                            </Card>
                        </Col>
                        <Col sm={4}>
                            <Card>
                                <Card.Body>
                                <Card.Title><h3 className="text-center" style={{ color: '#4361EE' }}><NavLink style={{ color: '#4361EE' }} class="nav-link dropdown-toggle" to={`/employees/${id}`}><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-people" viewBox="0 0 16 16">
  <path d="M15 14s1 0 1-1-1-4-5-4-5 3-5 4 1 1 1 1h8zm-7.978-1A.261.261 0 0 1 7 12.996c.001-.264.167-1.03.76-1.72C8.312 10.629 9.282 10 11 10c1.717 0 2.687.63 3.24 1.276.593.69.758 1.457.76 1.72l-.008.002a.274.274 0 0 1-.014.002H7.022zM11 7a2 2 0 1 0 0-4 2 2 0 0 0 0 4zm3-2a3 3 0 1 1-6 0 3 3 0 0 1 6 0zM6.936 9.28a5.88 5.88 0 0 0-1.23-.247A7.35 7.35 0 0 0 5 9c-4 0-5 3-5 4 0 .667.333 1 1 1h4.216A2.238 2.238 0 0 1 5 13c0-1.01.377-2.042 1.09-2.904.243-.294.526-.569.846-.816zM4.92 10A5.493 5.493 0 0 0 4 13H1c0-.26.164-1.03.76-1.724.545-.636 1.492-1.256 3.16-1.275zM1.5 5.5a3 3 0 1 1 6 0 3 3 0 0 1-6 0zm3-2a2 2 0 1 0 0 4 2 2 0 0 0 0-4z"/>
</svg>
Employees</NavLink>  </h3></Card.Title>

                                    
                                    <Card.Text className="text-center">
                                        7
                                    </Card.Text>
                                    
                                </Card.Body>
                            </Card>
                        </Col>
                        <Col  sm={4}>
                            <Card>
                                <Card.Body>
                                <Card.Title><h3 className="text-center" style={{ color: '#4361EE' }}><NavLink style={{ color: '#4361EE' }} class="nav-link dropdown-toggle" to="#">
                                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-gift" viewBox="0 0 16 16">
  <path d="M3 2.5a2.5 2.5 0 0 1 5 0 2.5 2.5 0 0 1 5 0v.006c0 .07 0 .27-.038.494H15a1 1 0 0 1 1 1v2a1 1 0 0 1-1 1v7.5a1.5 1.5 0 0 1-1.5 1.5h-11A1.5 1.5 0 0 1 1 14.5V7a1 1 0 0 1-1-1V4a1 1 0 0 1 1-1h2.038A2.968 2.968 0 0 1 3 2.506V2.5zm1.068.5H7v-.5a1.5 1.5 0 1 0-3 0c0 .085.002.274.045.43a.522.522 0 0 0 .023.07zM9 3h2.932a.56.56 0 0 0 .023-.07c.043-.156.045-.345.045-.43a1.5 1.5 0 0 0-3 0V3zM1 4v2h6V4H1zm8 0v2h6V4H9zm5 3H9v8h4.5a.5.5 0 0 0 .5-.5V7zm-7 8V7H2v7.5a.5.5 0 0 0 .5.5H7z"/></svg>
Offers</NavLink>  </h3></Card.Title>




                                    <Card.Text className="text-center"> 
                                        9                    
                                    </Card.Text>                                
                                </Card.Body>
                            </Card>
                        </Col>

        </Row>
</div>
<div class="col-xl-12 col-lg-12 col-sm-12  layout-spacing">
<h4>Merchant Details:</h4>
<Row>
                        <Col sm={3}>
                      
                        <Table striped>
            <thead>
            <tr>
                        <th>Name</th>
                        <td>{name}</td>
                    </tr>
                    <tr>
                        <th>Mobile</th>
                        <td>{mobile}</td>
                    </tr>
                    <tr>
                        <th>User Cur</th>
                        <td>{userCurrncy}</td>
                    </tr>
                   
            </thead>

</Table>
                            </Col>
                            <Col sm={3}>
                        <Table striped>
            <thead>
       
                    <tr>
                        <th>Is Aproved</th>
                        <td>{isAproved}</td>
                    </tr>
                    <tr>
                        <th>Is Live</th>
                        <td>{isLive}</td>
                    </tr>
                    <tr>
                        <th>Balance</th>
                        <td>{balance}</td>
                    </tr>
            </thead>

</Table>
                            </Col>

                            <Col sm={3}>
<Table striped>
            <thead>
                    <tr>
                        <th>Hotline</th>
                        <td>{hotLine}</td>
                    </tr>
                    <tr>
                        <th>PointPrice</th>
                        <td>{pointPrice}</td>
                    </tr><tr>
                        <th>Currency</th>
                        <td>{currency}</td>
                    </tr>
                    
            </thead>

</Table>
                            </Col>

                            <Col sm={3}>
<Table striped>
            <thead>
                    
                    <tr>
                        <th>Commission %</th>
                        <td>{commissionPercentage}</td>
                    </tr>
                    <tr>
                        <th>Tax</th>
                        <td>{taxRegistrationNumber}</td>
                    </tr>
                    {/* <tr>
                        <th>Category</th>
                        <td>{categoryId}</td>
                    </tr> */}
            </thead>

</Table>
                            </Col>
                          
                           
        </Row>

</div>

</>
   


    {/* <div class="widget-content widget-content-area br-6">   
     
                <ReactDatatable
                    config={config}
                    records={element_top_branch}
                    columns={columnsBranches}
                    dynamic={false}
                    total_record={totalBranches}
                    loading={loading}
                    onChange={tableChangeHandler} />

    </div> */}
   
    
        <div class="col-sm-6">
                <div class="widget widget-table-three">

                        <div class="widget-heading">
                            <h5 class=""><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-shop" viewBox="0 0 16 16">
                            <path d="M2.97 1.35A1 1 0 0 1 3.73 1h8.54a1 1 0 0 1 .76.35l2.609 3.044A1.5 1.5 0 0 1 16 5.37v.255a2.375 2.375 0 0 1-4.25 1.458A2.371 2.371 0 0 1 9.875 8 2.37 2.37 0 0 1 8 7.083 2.37 2.37 0 0 1 6.125 8a2.37 2.37 0 0 1-1.875-.917A2.375 2.375 0 0 1 0 5.625V5.37a1.5 1.5 0 0 1 .361-.976l2.61-3.045zm1.78 4.275a1.375 1.375 0 0 0 2.75 0 .5.5 0 0 1 1 0 1.375 1.375 0 0 0 2.75 0 .5.5 0 0 1 1 0 1.375 1.375 0 1 0 2.75 0V5.37a.5.5 0 0 0-.12-.325L12.27 2H3.73L1.12 5.045A.5.5 0 0 0 1 5.37v.255a1.375 1.375 0 0 0 2.75 0 .5.5 0 0 1 1 0zM1.5 8.5A.5.5 0 0 1 2 9v6h1v-5a1 1 0 0 1 1-1h3a1 1 0 0 1 1 1v5h6V9a.5.5 0 0 1 1 0v6h.5a.5.5 0 0 1 0 1H.5a.5.5 0 0 1 0-1H1V9a.5.5 0 0 1 .5-.5zM4 15h3v-5H4v5zm5-5a1 1 0 0 1 1-1h2a1 1 0 0 1 1 1v3a1 1 0 0 1-1 1h-2a1 1 0 0 1-1-1v-3zm3 0h-2v3h2v-3z"/>
                            </svg> Top 10 Branches 
                            
      

<NavLink  style={{float: "right"}} class="nav-link dropdown-toggle" to={`/branches/${id}`}><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-view-list" viewBox="0 0 16 16">
  <path d="M3 4.5h10a2 2 0 0 1 2 2v3a2 2 0 0 1-2 2H3a2 2 0 0 1-2-2v-3a2 2 0 0 1 2-2zm0 1a1 1 0 0 0-1 1v3a1 1 0 0 0 1 1h10a1 1 0 0 0 1-1v-3a1 1 0 0 0-1-1H3zM1 2a.5.5 0 0 1 .5-.5h13a.5.5 0 0 1 0 1h-13A.5.5 0 0 1 1 2zm0 12a.5.5 0 0 1 .5-.5h13a.5.5 0 0 1 0 1h-13A.5.5 0 0 1 1 14z"/>
</svg>
 </NavLink>

</h5>

                        </div>

                    <div class="widget-content">
                        <div class="table-responsive">
                            <table class="table table-scroll">
                                <thead>
                                    <tr>
                                        <th><div class="th-content th-heading">Name</div></th>
                                        <th><div class="th-content th-heading">Phone</div></th>
                                        <th><div class="th-content th-heading">Latitude</div></th>
                                        <th><div class="th-content th-heading">Longitude</div></th>

                                    </tr>
                                </thead>
                                <tbody>
                                    
                                
                                {element_top_branch_html}


                                </tbody>
                            </table>
                        </div>

                    </div>

                </div>

        </div>
        {/* <div class="col-sm-6">
    <div class="widget widget-table-three">

<div class="widget-heading">
    <h5 class=""><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-people" viewBox="0 0 16 16">
  <path d="M15 14s1 0 1-1-1-4-5-4-5 3-5 4 1 1 1 1h8zm-7.978-1A.261.261 0 0 1 7 12.996c.001-.264.167-1.03.76-1.72C8.312 10.629 9.282 10 11 10c1.717 0 2.687.63 3.24 1.276.593.69.758 1.457.76 1.72l-.008.002a.274.274 0 0 1-.014.002H7.022zM11 7a2 2 0 1 0 0-4 2 2 0 0 0 0 4zm3-2a3 3 0 1 1-6 0 3 3 0 0 1 6 0zM6.936 9.28a5.88 5.88 0 0 0-1.23-.247A7.35 7.35 0 0 0 5 9c-4 0-5 3-5 4 0 .667.333 1 1 1h4.216A2.238 2.238 0 0 1 5 13c0-1.01.377-2.042 1.09-2.904.243-.294.526-.569.846-.816zM4.92 10A5.493 5.493 0 0 0 4 13H1c0-.26.164-1.03.76-1.724.545-.636 1.492-1.256 3.16-1.275zM1.5 5.5a3 3 0 1 1 6 0 3 3 0 0 1-6 0zm3-2a2 2 0 1 0 0 4 2 2 0 0 0 0-4z"/>
</svg>Top 10 Employees <a href="#" style={{float: "right"}}>View all</a></h5>
</div>

<div class="widget-content">
    <div class="table-responsive">
        <table class="table table-scroll">
            <thead>
                <tr>
                    <th><div class="th-content">First name</div></th>
                    <th><div class="th-content th-heading">Last name</div></th>
                    <th><div class="th-content th-heading">Mobile</div></th>
                    <th><div class="th-content">Rule</div></th>

                </tr>
            </thead>
            <tbody>
                
               
              {element_top_employee_html}


            </tbody>
        </table>
    </div>
</div>
</div>
        </div>
              */}
   
    {/* <h2>Top 10 Employees</h2>

    <div class="widget-content widget-content-area br-6">   
     
                <ReactDatatable
                    config={config}
                    records={element_top_employee}
                    columns={Employeecolumns}
                    dynamic={false}
                    total_record={totalEmployee}
                    loading={loading}
                onChange={tableChangeHandler} />

    </div> */}
    

</div>            
                </div>            
            <Footer />
        </div>
    </div>
    </div>
);
                    }

 export default MerchantDetails;
