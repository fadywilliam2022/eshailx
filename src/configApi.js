export default {
    headers: {
        token: localStorage.getItem('token'),
        'Accept': 'application/json',
        // "Access-Control-Allow-Origin": "*",
        // "Access-Control-Allow-Headers": "X-Requested-With",
        "Access-Control-Allow-Methods": "GET, POST, PATCH, PUT, DELETE, OPTIONS",
        "Access-Control-Allow-Headers": "X-Requested-With, Origin, Content-Type, X-Auth-Token, Authorization"

    }
};
