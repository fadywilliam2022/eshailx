import React from 'react';
// import ReactDOM from 'react-dom';
import { render } from "react-dom";
import {
  BrowserRouter,
  Routes,
  Route
} from "react-router-dom";
import './index.css';
import App from './App';
import Login from './components/Login';

import reportWebVitals from './reportWebVitals';
import { Provider } from 'react-redux';
import { store } from './redux/storeConfig/store'
import Category from './components/category/Category';
import Merchant from './components/merchant/Merchant';
import MerchantDetails from './components/merchant/MerchantDetails';
import Branch from './components/branch/Branch';
import Employee from './components/employee/Employee';
import BranchDetails from './components/branch/BranchDetails';
import User from './components/user/User';
import REQ from './components/req/Req';



render(
  <React.StrictMode>
  <Provider store={store}>
    <BrowserRouter>
    <Routes>
  
      <Route path="/login" element={<Login />} />
     
    </Routes>
    <Routes>
      <Route exact={true} path="/" element={<App />} />
      <Route exact={true} path="dashboard" element={<App />} />
      <Route exact={true} path="/users/" element={<User />} />
      {/* <Route path="/login" element={<Login />} /> */}
      <Route exact={true} path="/categories/" element={<Category />} />
      <Route exact={true} path="/merchants/" element={<Merchant />} />
      <Route exact={true} path="/reqs/" element={<REQ />} />

      <Route  path="/merchantdetails/:id" element={<MerchantDetails />} />
      <Route  path="/branches/:id" element={<Branch />} />
      <Route  path="/employees/:id" element={<Employee />} />
      <Route  path="/branchdetails/:id/:merchantId" element={<BranchDetails />} /> 

      



      
    </Routes>
    </BrowserRouter>
    </Provider>
    </React.StrictMode>,
  document.getElementById('root')
);
reportWebVitals();


// ReactDOM.render(
//   <React.StrictMode>
//     <App />
//   </React.StrictMode>,
//   document.getElementById('root')
// );

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
