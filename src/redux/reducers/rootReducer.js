// ** Redux Imports
import { combineReducers } from 'redux'

// ** Reducers Imports
// import auth from './../reducers/auth'

import eshailx from '../../store/reducer'

import auth from "../../store/reducer/auth";
import message from "../../store/reducer/message";

const rootReducer = combineReducers({
    auth ,
    message,
    eshailx
})

export default rootReducer
