import axios from "axios";
import { history } from "../helpers/history";

const register = (username, email, password) => {
  return axios.post(`${process.env.REACT_APP_PUBLIC_URL}signup`, {
    username,
    email,
    password,
  });
};

const login = (username,password) => {
  console.log('login api')
  // const email = userData.email;
  // const password = userData.password;
  // console.log(password);
  return axios
    .post(`${process.env.REACT_APP_PUBLIC_URL}login`,{
      username,
      password,
    })
    .then((response) => {
      // if (response.data.accessToken) {
        if (response.data.data.token) {
        localStorage.setItem("user", JSON.stringify(response.data));
        localStorage.setItem("token", response.data.data.token);
       
        history.push('/');
        window.location.reload();
      }else{
        // history.push('/login');
        // window.location.reload();
      }

      return response.data;
    });
};

const logout = () => {
  localStorage.removeItem("user");
};

const getCurrentUser = () => {
  return JSON.parse(localStorage.getItem("user"));
};

export default {
  register,
  login,
  logout,
  getCurrentUser,
};