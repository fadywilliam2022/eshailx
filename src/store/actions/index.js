import axios from 'axios'
// import configApi from "../../configApi";
// import { handleLogout } from '../../redux/actions/auth'
import { history } from "../../helpers/history";
import { SET_MESSAGE, CLEAR_MESSAGE } from "../actions/types";

export const listCategory = (token,pageNum) => {
  return async dispatch => {
   
    await axios.post(`${process.env.REACT_APP_PUBLIC_URL}catigory/list`, 
    {token,pageNum},
    ).then(response => {      
      dispatch({
        type: 'GET_LIST_CATEGORIES',
        data: response.data
      })
    
    },  err => {
     history.push('/login');
     window.location.reload();
       localStorage.removeItem("user");
       localStorage.removeItem("token");

      
    })
  }
}
export const listUser = (token,pageNum) => {
  return async dispatch => {
   
    await axios.post(`${process.env.REACT_APP_PUBLIC_URL}users/list`, 
    {token,pageNum},
    ).then(response => {      
      dispatch({
        type: 'GET_LIST_USER',
        data: response.data
      })
    
    },  err => {
     history.push('/login');
     window.location.reload();
       localStorage.removeItem("user");
       localStorage.removeItem("token");

      
    })
  }
}
export const listReq = (token,pageNum) => {
  return async dispatch => {
   
    await axios.post(`${process.env.REACT_APP_PUBLIC_URL}req/list`, 
    {token,pageNum},
    ).then(response => {      
      dispatch({
        type: 'GET_LIST_REQ',
        data: response.data
      })
    
    },  err => {
     history.push('/login');
     window.location.reload();
       localStorage.removeItem("user");
       localStorage.removeItem("token");

      
    })
  }
}


export const addUser = (token,name,mobile,password,userCurrncy) => {
 
  return async (dispatch) => {   
    await axios.post(`${process.env.REACT_APP_PUBLIC_URL}user/create`,     
    {token,name,mobile,password,userCurrncy},
    ).then(response => {      
      dispatch({
        type: ' ',
        data: response.data
      })
    
    },  err => {
     
    
      
    })
  }
}

export const deleteCategory = (token,id) => {
  return async dispatch => {
   
    await axios.post(`${process.env.REACT_APP_PUBLIC_URL}catigory/delete`, 
    {token,id},
    ).then(response => {      
      dispatch({
        type: 'DELETE_CATEGORY',
        data: response.data
      })
    
    },  err => {
     

      
    })
  }
}
export const addCategory = (token,name,nameAr) => {
 
  return async (dispatch) => {   
    await axios.post(`${process.env.REACT_APP_PUBLIC_URL}catigory/create`,     
    {token,name,nameAr},
    ).then(response => {      
      dispatch({
        type: 'ADD_CATEGORY',
        data: response.data
      })
    
    },  err => {
     
    
      
    })
  }
}

export const updateCategory = (token,id,name,nameAr) => {
 
  return async (dispatch) => {   
    await axios.post(`${process.env.REACT_APP_PUBLIC_URL}catigory/update`,     
    {token,id,name,nameAr},
    ).then(response => {      
      dispatch({
        type: 'UPDATE_CATEGORY',
        data: response.data
      })
    
    },  err => {
     

      
    })
  }
}
/// merchant
export const listMerchant = (token,pageNum) => {
  return async dispatch => {
   
    await axios.post(`${process.env.REACT_APP_PUBLIC_URL}merchant/list`, 
    {token,pageNum},
    ).then(response => {      
      dispatch({
        type: 'GET_LIST_MERCHANTS',
        data: response.data
      })
    
    },  err => {
       history.push('/login');
       window.location.reload();
       localStorage.removeItem("user");
       localStorage.removeItem("token");


      
    })
  }
}

export const signupMerchant = (name,mobile,password,userCurrncy) => {
  
  return async dispatch => {
    await axios.post(`${process.env.REACT_APP_URL}merchant/signup`, 
    {name,mobile,password,userCurrncy},
    ).then(response => {     
     
      dispatch({
        type: 'SIGN_UP_MERCHANTS',
        data: response.data
      })
      
      dispatch({
        type: SET_MESSAGE,
        payload: "Success signup",
      });
    },  error => {
     
      const message =
      (error.response &&
      error.response.data &&
      error.response.data.message) ||
      error.message ||
      error.toString();
    //  console.log(message)
    //  alert(message)
    dispatch({
      type: SET_MESSAGE,
      payload: message,
    });
  //  alert(message)
    })
  }
}
export const CreateMerchantProfile = (token,merchantId ,hotLine,pointPrice,currency,commissionPercentage,taxRegistrationNumber,categoryId) => {
  return async dispatch => {
    await axios.post(`${process.env.REACT_APP_PUBLIC_URL}merchant/create/merchantProfile`, 
    {token,merchantId,hotLine,pointPrice,currency,commissionPercentage,taxRegistrationNumber,categoryId},
    ).then(response => {      
      dispatch({
        type: 'CREATE_MERCHANT_PROFILE',
        data: response.data
      })
    
    },  error => {
     
      const message =
      (error.response &&
      error.response.data &&
      error.response.data.message) ||
      error.message ||
      error.toString();
      dispatch({
        type: SET_MESSAGE,
        payload: message,
      });
    })
  }
}


export const approveMerchant = (token,profileId) => {
  return async dispatch => {
    await axios.post(`${process.env.REACT_APP_PUBLIC_URL}merchant/approve/merchantProfile`, 
    {token,profileId},
    ).then(response => {      
      dispatch({
        type: 'APPROVE_MERCHANT',
        data: response.data
      })
    
    },  error => {
     
      const message =
      (error.response &&
      error.response.data &&
      error.response.data.message) ||
      error.message ||
      error.toString();
      dispatch({
        type: SET_MESSAGE,
        payload: message,
      });
    })
  }
}

export const merchantDetails = (token,merchantId) => {
  return async dispatch => {
    await axios.post(`${process.env.REACT_APP_PUBLIC_URL}merchant/details`, 
    {token,merchantId},
    ).then(response => {      
      dispatch({
        type: 'MERCHANT_DETAILS',
        data: response.data
      })
    
    },  error => {
     
      const message =
      (error.response &&
      error.response.data &&
      error.response.data.message) ||
      error.message ||
      error.toString();
      dispatch({
        type: SET_MESSAGE,
        payload: message,
      });
    })
  }
}
export const listMerchantbranches = (token,merchantId) => {
  return async dispatch => {
    await axios.post(`${process.env.REACT_APP_PUBLIC_URL}merchant/branch/list`, 
    {token,merchantId},
    ).then(response => {      
      dispatch({
        type: 'MERCHANT_BRANCH_LIST',
        data: response.data
      })
    
    },  error => {
     
      const message =
      (error.response &&
      error.response.data &&
      error.response.data.message) ||
      error.message ||
      error.toString();
      dispatch({
        type: SET_MESSAGE,
        payload: message,
      });
    })
  }
}
export const listMerchantemployee = (token,merchantId) => {
  return async dispatch => {
    await axios.post(`${process.env.REACT_APP_PUBLIC_URL}merchant/user/list`, 
    {token,merchantId},
    ).then(response => {      
      dispatch({
        type: 'MERCHANT_USER_LIST',
        data: response.data
      })
    
    },  error => {
     
      const message =
      (error.response &&
      error.response.data &&
      error.response.data.message) ||
      error.message ||
      error.toString();
      dispatch({
        type: SET_MESSAGE,
        payload: message,
      });
    })
  }
}

export const listEmoloyees = (token,branchId,merchantId) => {
  return async dispatch => {
    await axios.post(`${process.env.REACT_APP_PUBLIC_URL}merchant/employees/list`, 
    {token,branchId,merchantId},
    ).then(response => {      
      dispatch({
        type: 'EMPLOYEE_LIST',
        data: response.data
      })
    
    },  error => {
     
      const message =
      (error.response &&
      error.response.data &&
      error.response.data.message) ||
      error.message ||
      error.toString();
      dispatch({
        type: SET_MESSAGE,
        payload: message,
      });
    })
  }
}

export const addBranch = (token,merchantId,branchName,phone,lat,long,address) => {
  return async dispatch => {
    await axios.post(`${process.env.REACT_APP_PUBLIC_URL}addBranch`, 
    {token,merchantId,branchName,phone,lat,long,address}
    ).then(response => {      
      dispatch({
        type: 'ADD_BRANCH',
        data: response.data
      })
    
    },  error => {
    console.log('rr')
      const message =
      (error.response &&
      error.response.data &&
      error.response.data.message) ||
      error.message ||
      error.toString();
      dispatch({
        type: SET_MESSAGE,
        payload: message,
      });
    })
  }
}
export const addEmployee = (token,branchId,mobile,password,fName,lName,rule) => {
  return async dispatch => {
    await axios.post(`${process.env.REACT_APP_PUBLIC_URL}merchant/employees/add`, 
    {token,branchId,mobile,password,fName,lName,rule},
    ).then(response => {      
      dispatch({
        type: 'ADD_EMPLOYEE',
        data: response.data
      })
    
    },  error => {
      const message =
      (error.response &&
      error.response.data &&
      error.response.data.message) ||
      error.message ||
      error.toString();
      dispatch({
        type: SET_MESSAGE,
        payload: message,
      });
    })
  }
}

export const getBranchDetails = (token,id) => {
  return async dispatch => {
    await axios.post(`${process.env.REACT_APP_URL}merchant/branch/details`, 
    {token,id},
    ).then(response => {      
      dispatch({
        type: 'GET_BRANCH_DETAILS',
        data: response.data
      })
    
    },  error => {
      const message =
      (error.response &&
      error.response.data &&
      error.response.data.message) ||
      error.message ||
      error.toString();
      dispatch({
        type: SET_MESSAGE,
        payload: message,
      });
    })
  }
}



