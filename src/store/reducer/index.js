// ** Initial State
const initialState = {
    info: [],
    categories :[],
    add_cat:[],
    del_cat:[],
    up_cat :[],
    merchants:[],
    sign_up_merchant:[],
    create_merchant_profile:[],
    approve_merchant:[],
    merchant_details:[],
    merchant_branch_list:[],
    add_branch:[],
    add_employee:[],
    merchant_user_list:[],
    branch_details:[],
    employee_list:[],
    user_list:[],
    add_user:[],
    list_req:[]
  }
  
  const eshailxReducer = (state = initialState, action) => {
    switch (action.type) {
      // case 'GET_DATA_INFO':
      //   return {
      //     ...state,     
      //     info: action.data
      //   }   
          case 'GET_LIST_CATEGORIES':
          return {
          ...state,     
          categories: action.data
          } 
          case 'ADD_CATEGORY':
          return {
          ...state,     
          add_cat: action.data
          }    
          case 'DELETE_CATEGORY':
          return {
            ...state,     
            del_cat: action.data
          }  
          case 'UPDATE_CATEGORY':
            return {
              ...state,     
              up_cat: action.data
            }       
          case 'GET_LIST_MERCHANTS':
          return {
            ...state,     
            merchants: action.data
          } 
          case 'SIGN_UP_MERCHANTS':
            return {
              ...state,     
              sign_up_merchant: action.data
            } 
          case 'CREATE_MERCHANT_PROFILE':
            return {
              ...state,     
              create_merchant_profile: action.data
            }

            case 'APPROVE_MERCHANT':
              return {
                ...state,     
                approve_merchant: action.data
              }

              case 'MERCHANT_DETAILS':
                return {
                  ...state,     
                  merchant_details: action.data
                }
                case 'MERCHANT_BRANCH_LIST':
                  return {
                    ...state,     
                    merchant_branch_list: action.data
                  }
                  case 'ADD_BRANCH':
                  return {
                    ...state,     
                    add_branch: action.data
                  }
                  case 'ADD_EMPLOYEE':
                    return {
                      ...state,     
                      add_employee: action.data
                    }
                    case 'MERCHANT_USER_LIST':
                      return {
                        ...state,     
                        merchant_user_list: action.data
                      }

                      case 'GET_BRANCH_DETAILS':
                        return {
                          ...state,     
                          branch_details: action.data
                        }

                      case 'EMPLOYEE_LIST':
                        return {
                          ...state,     
                          employee_list: action.data
                        }
                        case 'GET_LIST_USER':
                          return {
                            ...state,     
                            user_list: action.data
                          }
                          case 'ADD_USER':
                            return {
                              ...state,     
                              add_user: action.data
                            }
                            case 'GET_LIST_REQ':
                              return {
                                ...state,     
                                list_req: action.data
                              }
                            
                        
                    
                  
      default:
        return state
    }
  }
  
  export default eshailxReducer
  